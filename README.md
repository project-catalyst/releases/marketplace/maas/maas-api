# H2020 CATALYST Marketplace as a Service API

## What it is
Enables building Marketplace instances to Kubernetes clusters. More info will follow.


## How to build
To build use the following commands:

```bash
cd <download_dir>
cd build/
# Build it
docker-compose build -f docker-compose.yaml
# Log into dockerhub
docker login -u powerops
# Push it
docker push powerops/catalyst-maas-api:<tag_version>
docker push powerops/catalyst-maas-api:latest
```

## How to deploy
Copy the `docker-compose.yaml` and `.env` files located under the `docker` directory to a 
server supporting docker and docker-compose. Then, edit the .env file and issue the command

```bash
docker-copmpose up -d
```

## Important URLs
The API docs are located under `http://<host>/catalyst/maas/api/v1/docs`.

