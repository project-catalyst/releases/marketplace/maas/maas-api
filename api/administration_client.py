from dataclasses import dataclass
import logging
from django.conf import settings 

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")



@dataclass
class AdministrationClient:

    def __post_init__(self):
        self.stage = -1
        self.complete = False

    def __get_function(self, name):
        for attr in dir(self):
            if attr.endswith(name) and callable(getattr(self, attr)):
                return getattr(self, attr)

    def __get_step_function(self, step_name):
        step_function = self.__get_function(f"__step_{step_name}")
        if not step_function:
            log.error(f"{self.__class__.__name__} does not have step {step_name}")
            raise AttributeError(
                f"{self.__class__.__name__} does not have step {step_name}"
            )
        return step_function

    def __get_undo_function(self, step_name):
        return self.__get_function(
            f"__undo_{step_name}"
        )

    def is_complete(self):
        return self.complete

    def deploy(self):
        log.info(f"Starting deployment of {self.__class__.__name__}")
        for i, step in enumerate(self.__class__.steps):
            self.stage = i
            step_func = self.__get_step_function(step)
            if not step_func:
                log.error(f"")
            log.info(f"Step: {step}")
            try:
                step_func()
            except Exception as e:

                log.error(f"Step {step} failed; Rolling back deployment")

                self.destroy()
                if getattr(self.__class__, "raise_exc", True):
                    raise e
                    break
                else:
                    import traceback
                    log.error(traceback.format_exc())

        self.complete = True

    def destroy(self):
        log.info(f"Rolling back deployment of {self.__class__.__name__}")
        self.complete = False
        for step in reversed(self.__class__.steps[:self.stage]):
            undo_step = self.__get_undo_function(step)
            if undo_step:
                log.info(f"Undoing step: {step}")
                try:
                    undo_step()
                except Exception as e:
                    import traceback
                    log.error(f"Undo failed:{traceback.format_exc()}")
                