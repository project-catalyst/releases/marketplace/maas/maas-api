import json
import logging
import logging.config

from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from keycloak.exceptions import KeycloakAuthenticationError
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from api.serializers import CredentialsSerializer, RefreshTokenSerializer
from auth.clients.keycloak.api import Client


class TokenRevocation(CreateAPIView):
    keycloak_scopes = {
        'POST': 'maas-api-write'
    }
    serializer_class = RefreshTokenSerializer

    @property
    def log(self):
        logging.config.dictConfig(settings.LOGGING)
        return logging.getLogger("api")

    @property
    def client(self):
        return Client().get_client()

    @swagger_auto_schema()
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            refresh_token = serializer.validated_data['refresh_token']
            self.client.logout(refresh_token)
            return Response()
        except Exception as e:
            self.log.exception("Could not logout user.", e)
            return Response({"detail": "Could not logout user"},
                            status=500)


class TokenGeneration(ObtainAuthToken):
    serializer_class = CredentialsSerializer

    @property
    def log(self):
        logging.config.dictConfig(settings.LOGGING)
        return logging.getLogger("api")

    @property
    def client(self):
        return Client().get_client()

    @swagger_auto_schema(request_body=CredentialsSerializer)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data['username']
        password = serializer.validated_data['password']

        try:
            # Get the token and user info from Keycloak
            token = self.client.token(username=username,
                                      password=password)
            info = self.client.userinfo(token['access_token'])

            # Remove unnecessary fields
            token.pop("not-before-policy")
            token.pop("session_state")
            info.pop("email_verified")

            return Response({
                "auth": token,
                "info": info
            })
        except KeycloakAuthenticationError as e:
            self.log.exception("Keycloak error", e)
            return Response(status=e.response_code,
                            data=json.loads(e.response_body))
        except Exception as ex:
            self.log.exception("Exception while processing token request.", ex)
            return Response(status=500,
                            data={"details": "{}".format(ex)})
