import time

from api.models import Region, Flavour
from .utils import format_namespace, get_variant_form, get_variant_type, encode_secret, generate_password, \
    get_memo_service_url, get_ip_and_port, get_am_external_url, get_ib_external_url
from dataclasses import dataclass
from kubernetes import client as k8sclient, config
from kubernetes.client import AppsV1Api, CoreV1Api, NetworkingV1beta1Api, BatchV1Api
from api.administration_client import AdministrationClient
from .deployment_info import VariantDeploymentInfo, VariantDeploymentResult, MarketplaceDeploymentInfo, \
    MarketplaceDeploymentResult

from k8s.KObjects import \
    Namespace, \
    ConfigMap, \
    Deployment, \
    Ingress, \
    PersistentVolume, \
    PersistentVolumeClaim, \
    Service, \
    Secret, Job

import logging
from django.conf import settings


logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")

@dataclass
class DeploymentClient(AdministrationClient):
    namespace: str

    def __post_init__(self):
        super().__post_init__()
        config.load_kube_config(config_file="/kubectl_files/kubeconfig.yaml")
        config.verify_ssl = False
        self.appsV1client = k8sclient.AppsV1Api()
        self.coreV1client = k8sclient.CoreV1Api()
        self.netV1client = k8sclient.NetworkingV1beta1Api()
        self.batchV1client = k8sclient.BatchV1Api()

         
@dataclass
class MarketDeploymentClient(DeploymentClient):
    marketplace_deployment_info: MarketplaceDeploymentInfo
    """
        Create a new namespace with the name {country}-{region}-{city}
        Create an Access Manager deployment
        Create an Access Manager service
        Create an Access Manager ingress rule
        Create PV & PVC for memo
        Deploy MEMO DB
        Create a MEMO deployment
    """

    steps = [
        "create_namespace",
        "create_marketplace_config_map",
        "create_memo_db_pv",
        "create_memo_db_pvc",
        "create_memo_config_map",
        "create_memo_secret",
        "deploy_memo_db",
        "create_memo_database_service",
        "initialize_memo_db",
        "deploy_access_manager",
        "create_access_manager_service",
        "create_access_manager_ingress_rule",
        "deploy_memo",
        "create_memo_service",
        "create_memo_ingress_rule"
    ]

    def delete_job(self, name):
        self.batchV1client.delete_namespaced_job(namespace=self.namespace, name=name)
        for pod in self.coreV1client.list_namespaced_pod(namespace=self.namespace).items:
            if name in pod.metadata.name:
                self.coreV1client.delete_namespaced_pod(namespace=self.namespace, name=pod.metadata.name)
                break
            else:
                continue
        log.info("Deleted job {}".format(name))

    def get_memo_url(self):
        return get_memo_service_url(self.namespace)

    def deploy(self):
        log.info(f"{self.namespace} - Starting market deployment")
        super().deploy()
        return MarketplaceDeploymentResult(
            am_url=get_am_external_url(self.namespace),
            memo_url=get_memo_service_url(self.namespace)
        )

    def destroy(self):
        deletion_client = MarketplaceDeploymentDeletionClient(
            self.namespace,
        )
        deletion_client.deploy()

    def __step_create_namespace(self):
        namespace = Namespace(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/namespaces/namespace.yaml"
        )
        self.coreV1client.create_namespace(body=namespace.get_object())

    def __step_create_marketplace_config_map(self):
        configmap = ConfigMap(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/configmaps/map.yaml"
        )
        slarc_ip, slarc_port = get_ip_and_port(self.marketplace_deployment_info.slarc_url)
        vcg_ip, vcg_port = get_ip_and_port(self.marketplace_deployment_info.vcg_url)
        dcmcm_ip, dcmcm_port = get_ip_and_port(self.marketplace_deployment_info.dcmcm_url)
        configmap.insert_attributes({
            "data.MULTIPLE_ENERGY_MARKETPLACE_ORCHESTRATOR_URL": self.get_memo_url(),
            "data.SLARC_IP": slarc_ip ,
            "data.SLARC_PORT": slarc_port,
            "data.VCG_IP": vcg_ip,
            "data.VCG_PORT": vcg_port,
            "data.ITLB_URL": self.marketplace_deployment_info.itlb_url,
            "data.DCMCM_IP": dcmcm_ip,
            "data.DCMCM_PORT": dcmcm_port
        })
        self.coreV1client.create_namespaced_config_map(
            namespace=self.namespace,
            body=configmap.get_object()
        )

    def __step_create_memo_config_map(self):
        configmap = ConfigMap(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/configmaps/memo-db-init.yaml"
        )
        self.coreV1client.create_namespaced_config_map(
            namespace=self.namespace,
            body=configmap.get_object()
        )

    def __step_create_memo_secret(self):
        secret = Secret(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/secrets/memo-database.yaml",
            metadata_name="memo-database-credentials",
        )
        self.coreV1client.create_namespaced_secret(
            namespace=self.namespace,
            body=secret.get_object()
        )

    def __step_create_memo_db_pv(self):
        pv = PersistentVolume(
            yaml_path="k8s/yaml/marketplace/storage/marketplace-memo-db-pv.yaml",
            namespace=None,
            metadata_name=f"{self.namespace}-memo-database-pv",
            host_path=f"/data/{self.namespace}/memo-database-pv"
        )
        self.coreV1client.create_persistent_volume(
            body=pv.get_object()
        )

    def __step_create_memo_db_pvc(self):
        pv=PersistentVolumeClaim(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/storage/marketplace-memo-db-pvc.yaml",
            metadata_name="memo-database-pvc",
            volume_name=f"{self.namespace}-memo-database-pv" 
        )
        self.coreV1client.create_namespaced_persistent_volume_claim(
            namespace=self.namespace,
            body=pv.get_object()
        )

    def __step_deploy_memo_db(self):
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/deployments/marketplace-memo-db.yaml"
        )
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )

    def __step_initialize_memo_db(self):
        job = Job(
            namespace=self.namespace,
            metadata_name="memo-init-db",
            yaml_path="k8s/yaml/marketplace/jobs/memo-database-init.yaml"
        )
        self.batchV1client.create_namespaced_job(
            namespace=self.namespace,
            body=job.get_object()
        )
        while True:
            all_ok = False
            for job in self.batchV1client.list_namespaced_job(namespace=self.namespace).items:
                if job.metadata.name != "memo-init-db":
                    continue
                if job.status.succeeded == 1:
                    all_ok = True
                    break
                else:
                    log.info("Waiting for MEMO DB init to complete. Current status is: succeeded = {}".format(job.status.succeeded))
                    time.sleep(1)
            if all_ok:
                self.delete_job(name="memo-init-db")
                break
    
    def __step_deploy_memo(self):
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/deployments/marketplace-memo.yaml"
        )
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )

    def __step_create_memo_service(self):
        service = Service(
            yaml_path="k8s/yaml/marketplace/services/marketplace-memo.yaml",
            namespace=self.namespace,
            metadata_name="memo-service"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )

    def __step_create_memo_database_service(self):
        service = Service(
            yaml_path="k8s/yaml/marketplace/services/marketplace-memo-database.yaml",
            namespace=self.namespace,
            metadata_name="memo-database-service"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )

    def __step_deploy_access_manager(self):
        memo_url = self.get_memo_url()
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/deployments/marketplace-am.yaml"
        )
        deployment.add_env_to_container(container_name="access-manager", env={
            "name":"MULTIPLE_ENERGY_MARKETPLACE_ORCHESTRATOR_URL",
            "value": "memo-service/memo/rest"
        })
        deployment.add_env_to_container(container_name="access-manager", env={
            "name":"RELATIVE_URL",
            "value": f"catalyst/marketplace/{self.namespace}"
        })
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )

    def __step_create_access_manager_service(self):
        service = Service(
            yaml_path="k8s/yaml/marketplace/services/marketplace-am.yaml",
            namespace=self.namespace,
            metadata_name="access-manager-service"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )

    def __step_create_access_manager_ingress_rule(self):
        ingress = Ingress(
            namespace=self.namespace,
            metadata_name="access-manager-ingress",
            yaml_path="k8s/yaml/marketplace/ingress/am.yaml",
        )
        ingress.insert_attribute(
            "spec.rules.0.http.paths.0.path",
            f"/catalyst/marketplace/{self.namespace}/am(/|$)(.*)",
        )
        ingress.insert_attribute(
            "metadata_annotations_nginx.ingress.kubernetes.io/rewrite-target",
            f"/catalyst/marketplace/{self.namespace}/am/$2",
            sep="_"
        )
        self.netV1client.create_namespaced_ingress(
            namespace=self.namespace,
            body=ingress.get_object()
        )

    def __step_create_memo_ingress_rule(self):
        ingress = Ingress(
            namespace=self.namespace,
            metadata_name="memo-ingress",
            yaml_path="k8s/yaml/marketplace/ingress/memo.yaml",
        )
        ingress.insert_attribute(
            "spec.rules.0.http.paths.0.path",
            f"/catalyst/marketplace/{self.namespace}/memo(/|$)(.*)",
        )
        ingress.insert_attribute(
            "metadata_annotations_nginx.ingress.kubernetes.io/rewrite-target",
            "/memo/rest/$2",
            sep="_"
        )
        self.netV1client.create_namespaced_ingress(
            namespace=self.namespace,
            body=ingress.get_object()
        )

    def __step_migrate_access_manager(self):
        job = Job(
            namespace=self.namespace,
            metadata_name="access-manager-migrations",
            yaml_path="k8s/yaml/marketplace/jobs/am-migrate.yaml"
        )
        self.batchV1client.create_namespaced_job(
            namespace=self.namespace,
            body=job.get_object()
        )

@dataclass
class VariantDeploymentClient(DeploymentClient):
    """
        Create a PV for the information broker (IB) database
        Create a PVC for the IB database
        Create config maps holding the existing env vars and the variant as described in #4
        Create secrets holding the existing env vars and the AAA info from above
        Create the IB deployment passing the newly created config maps and secrets
        Do the same for the IB DB.
        Create the MCM deployment
        Do the same for the MBM
        Create services for IB, IB DB, MCM and MBM.
        Create an ingress rule for IB
    """
    variant_deployment_info: VariantDeploymentInfo

    steps = [
        "create_pv_for_ib_database",
        "create_pvc_for_ib_database", 
        "create_variant_configmap",
        "create_ib_secret",
        "create_ib_db_secret",
        "deploy_ib",
        "deploy_ib_database",
        "deploy_mcm",
        "deploy_msm",
        "create_ib_service",
        "create_ib_database_service",
        "create_ib_jobs_migrate",
        "create_ib_jobs_create_super_user",
        "create_ib_jobs_init_db",
        "create_mcm_service",
        "create_mbm_service",
        "create_ib_ingress_rule",
        "get_ib_service_url"
    ]

    def delete_job(self, name):
        self.batchV1client.delete_namespaced_job(namespace=self.namespace, name=name)
        for pod in self.coreV1client.list_namespaced_pod(namespace=self.namespace).items:
            if name in pod.metadata.name:
                self.coreV1client.delete_namespaced_pod(namespace=self.namespace, name=pod.metadata.name)
                break
            else:
                continue
        log.info("Deleted job {}".format(name))

    def deploy(self):
        log.info(f"{self.namespace} - Starting variant deployment")
        super().deploy()
        return VariantDeploymentResult(
            ib_url=get_ib_external_url(self.namespace, self.variant_deployment_info.variant_name)
        )

    def destroy(self):
        deletion_client = VariantDeploymentDeletionClient(
            self.namespace,
            self.variant_deployment_info.variant_name,
        )
        deletion_client.deploy()

    def __step_create_pv_for_ib_database(self):
        yaml_path = "k8s/yaml/marketplace/storage/marketplace-ib-{}-db-pv.yaml" \
            .format(self.variant_deployment_info.variant_name)
        pv=PersistentVolume(
            namespace=None,
            yaml_path=yaml_path,
            metadata_name="{}-information-broker-{}-database-pv" \
                .format(self.namespace, self.variant_deployment_info.variant_name),
            host_path=f"/data/{self.namespace}/information-broker-electricity-database-pv"
        )
        self.coreV1client.create_persistent_volume(
            body=pv.get_object()
        )

    def __step_create_pvc_for_ib_database(self):
        variant_name = self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/storage/marketplace-ib-{}-db-pvc.yaml" \
            .format(variant_name)
        pvc=PersistentVolumeClaim(
            namespace=self.namespace,
            yaml_path=yaml_path,
            metadata_name="information-broker-{}-database-pvc" \
                .format(variant_name),
            volume_name="{}-information-broker-{}-database-pv" \
                .format(self.namespace, variant_name)
        )
        self.coreV1client.create_namespaced_persistent_volume_claim(
            namespace=self.namespace,
            body=pvc.get_object()
        )
            
    def __step_create_variant_configmap(self):
        
        variant_name = self.variant_deployment_info.variant_name
        c = ConfigMap(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/configmaps/variant.yaml",
        )
        c.insert_attributes({
            "metadata.name": "{}-{}-config".format(self.namespace, variant_name),
            "data.TYPE": get_variant_type(self.variant_deployment_info.variant_name),
            "data.FORM": get_variant_form(self.variant_deployment_info.variant_name),
            "data.RELATIVE_URL": f"catalyst/marketplace/{self.namespace}/{variant_name}/information-broker/",
        })

        self.coreV1client.create_namespaced_config_map(
            namespace=self.namespace,
            body=c.get_object()
        )
       
    def __step_create_ib_secret(self):
        variant_name = self.variant_deployment_info.variant_name
        secret = Secret(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/secrets/marketplace-api.yaml",
            metadata_name=f"information-broker-{variant_name}-credentials"
        )
        secret_data = {
            "data.ADMIN_USERNAME": self.variant_deployment_info.admin_username,
            "data.ADMIN_PASSWORD": self.variant_deployment_info.admin_password,
            "data.AAA_URL": self.variant_deployment_info.aaa_url,
            "data.REALM": self.variant_deployment_info.realm,
            "data.CLIENT_ID": self.variant_deployment_info.client_id,
            "data.CLIENT_SECRET": self.variant_deployment_info.client_secret,
            "data.OS_AUTHZ_CONFIG": self.variant_deployment_info.os_authz_config,
            "data.REALM_ADMIN_USERNAME": self.variant_deployment_info.realm_admin_credentials["username"],
            "data.REALM_ADMIN_PASSWORD": self.variant_deployment_info.realm_admin_credentials["password"]
        }
        secret.insert_attributes(
            {key: encode_secret(val) for key,val in secret_data.items()}
        )
        self.coreV1client.create_namespaced_secret(
            namespace=self.namespace,
            body=secret.get_object()
        )

    def __step_create_ib_db_secret(self):
        variant_name = self.variant_deployment_info.variant_name
        secret = Secret(
            namespace=self.namespace,
            yaml_path="k8s/yaml/marketplace/secrets/marketplace-database.yaml",
            metadata_name=f"{variant_name}-database-credentials",
        )
        self.coreV1client.create_namespaced_secret(
            namespace=self.namespace,
            body=secret.get_object()
        )

    def __step_deploy_ib(self):
        variant_name = self.variant_deployment_info.variant_name
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/deployments/marketplace-ib-{variant_name}.yaml"
        )
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name":"TYPE",
            "valueFrom": {
                "configMapKeyRef":{
                    "name": f"{self.namespace}-{variant_name}-config",
                    "key": "TYPE"
                }
            }
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name":"FORM",
            "valueFrom": {
                "configMapKeyRef":{
                    "name": f"{self.namespace}-{variant_name}-config",
                    "key": "FORM"
                }
            }
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "POSTGRES_HOST",
            "value": f"information-broker-{variant_name}-database-service"
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name":"MULTIPLE_ENERGY_MARKETPLACE_ORCHESTRATOR_URL",
            "valueFrom": {
                "configMapKeyRef":{
                    "name": f"marketplace-config",
                    "key": "MULTIPLE_ENERGY_MARKETPLACE_ORCHESTRATOR_URL"
                }
            }
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "CLIENT_ID",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "CLIENT_ID"
                }
            }
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "CLIENT_SECRET",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "CLIENT_SECRET"
                }
            }
        })
        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "OS_AUTHZ_CONFIG",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "OS_AUTHZ_CONFIG"
                }
            }
        })

        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "REALM_ADMIN_USERNAME",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "REALM_ADMIN_USERNAME"
                }
            }
        })

        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "REALM_ADMIN_PASSWORD",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "REALM_ADMIN_PASSWORD"
                }
            }
        })

        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "KEYCLOACK_REALM_NAME",
            "valueFrom": {
                "secretKeyRef": {
                    "name": f"information-broker-{variant_name}-credentials",
                    "key": "REALM"
                }
            }
        })

        deployment.add_env_to_container(f"information-broker-{variant_name}", {
            "name": "RELATIVE_URL",
            "valueFrom": {
                "configMapKeyRef":{
                    "name": f"{self.namespace}-{variant_name}-config",
                    "key": "RELATIVE_URL"
                }
            }
        })
        
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )
            
    def __step_deploy_ib_database(self):
        variant_name = self.variant_deployment_info.variant_name
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/deployments/marketplace-ib-{variant_name}-db.yaml"
        )
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )
            
    def __step_deploy_mcm(self):
        variant_name = self.variant_deployment_info.variant_name
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/deployments/marketplace-mcm-{variant_name}.yaml"
        )
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )
            
    def __step_deploy_msm(self):
        variant_name = self.variant_deployment_info.variant_name
        deployment = Deployment(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/deployments/marketplace-mbm-{variant_name}.yaml"
        )
        self.appsV1client.create_namespaced_deployment(
            namespace=self.namespace,
            body=deployment.get_object()
        )
            
    def __step_create_ib_service(self):
        variant_name=self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/services/marketplace-ib-{}.yaml" \
            .format(variant_name)
        service = Service(
            yaml_path=yaml_path,
            namespace=self.namespace,
            metadata_name=f"information-broker-{variant_name}-service"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )
            
    def __step_create_ib_database_service(self):
        variant_name=self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/services/marketplace-ib-{}-db.yaml" \
            .format(variant_name)
        service = Service(
            yaml_path=yaml_path,
            namespace=self.namespace,
            metadata_name=f"information-broker-{variant_name}-database-service"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )
            
    def __step_create_mcm_service(self):
        variant_name=self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/services/marketplace-mcm-{}.yaml" \
            .format(variant_name)
        service = Service(
            yaml_path=yaml_path,
            namespace=self.namespace,
            metadata_name=f"market-clearing-manager-{variant_name}"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )
            
    def __step_create_mbm_service(self):
        variant_name=self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/services/marketplace-mbm-{}.yaml" \
            .format(variant_name)
        service = Service(
            yaml_path=yaml_path,
            namespace=self.namespace,
            metadata_name=f"market-billing-manager-{variant_name}"
        )
        self.coreV1client.create_namespaced_service(
            namespace=self.namespace,
            body=service.get_object()
        )
            
    def __step_create_ib_ingress_rule(self):
        variant_name=self.variant_deployment_info.variant_name
        yaml_path = "k8s/yaml/marketplace/ingress/marketplace-ib-{}.yaml" \
            .format(variant_name)
        ingress = Ingress(
            yaml_path=yaml_path,
            namespace=self.namespace,
            metadata_name=f"information-broker-{variant_name}-ingress",
        )
        ingress.insert_attribute(
            "spec.rules.0.http.paths.0.path",
            f"/catalyst/marketplace/{self.namespace}/{variant_name}/information-broker(/|$)(.*)"
        )
        ingress.insert_attribute(
            "metadata_annotations_nginx.ingress.kubernetes.io/rewrite-target",
            f"/catalyst/marketplace/{self.namespace}/{variant_name}/information-broker/$2",
            sep="_"
        )
        self.netV1client.create_namespaced_ingress(
            namespace=self.namespace,
            body=ingress.get_object()
        )

    def get_service_ip(self, service_name):
        service = self.coreV1client.read_namespaced_service_status(
            namespace=self.namespace,
            name=service_name
        )
        return service.spec.cluster_ip, service.spec.ports[0].port

    def __step_get_ib_service_url(self):
        variant_name=self.variant_deployment_info.variant_name
        service_name = f"information-broker-{variant_name}-service"
        ip, port = self.get_service_ip(service_name)
        self.ib_url = f"{ip}:{port}"

    def __step_create_ib_jobs_migrate(self):
        variant_name = self.variant_deployment_info.variant_name
        job = Job(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/jobs/ib-{variant_name}-migrate.yaml",
            metadata_name=f"information-broker-{variant_name}-migrations"
        )

        self.batchV1client.create_namespaced_job(
            namespace=self.namespace,
            body=job.get_object()
        )
        while True:
            done = False
            for job in self.batchV1client.list_namespaced_job(namespace=self.namespace).items:
                if job.metadata.name != f"information-broker-{variant_name}-migrations":
                    continue
                if job.status.succeeded == 1:
                    log.info("Migrations finished.")
                    done = True
                    break
                else:
                    log.info("Waiting for migrations to finish..")
                    time.sleep(1)
            if done:
                self.delete_job(name=f"information-broker-{variant_name}-migrations")
                break

    def __step_create_ib_jobs_create_super_user(self):
        variant_name = self.variant_deployment_info.variant_name
        job = Job(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/jobs/ib-{variant_name}-create_superuser.yaml",
            metadata_name=f"information-broker-{variant_name}-create-su"
        )

        self.batchV1client.create_namespaced_job(
            namespace=self.namespace,
            body=job.get_object()
        )
        while True:
            done = False
            for job in self.batchV1client.list_namespaced_job(namespace=self.namespace).items:
                if job.metadata.name != f"information-broker-{variant_name}-create-su":
                    continue
                if job.status.succeeded == 1:
                    log.info("SU created.")
                    done = True
                    break
                else:
                    log.info("Waiting for SU create to finish..")
                    time.sleep(1)
            if done:
                self.delete_job(name=f"information-broker-{variant_name}-create-su")
                break

    def __step_create_ib_jobs_init_db(self):
        variant_name = self.variant_deployment_info.variant_name
        job = Job(
            namespace=self.namespace,
            yaml_path=f"k8s/yaml/marketplace/jobs/ib-{variant_name}-init_db.yaml",
            metadata_name=f"information-broker-{variant_name}-init-db"
        )

        self.batchV1client.create_namespaced_job(
            namespace=self.namespace,
            body=job.get_object()
        )
        while True:
            done = False
            for job in self.batchV1client.list_namespaced_job(namespace=self.namespace).items:
                if job.metadata.name != f"information-broker-{variant_name}-init-db":
                    continue
                if job.status.succeeded == 1:
                    log.info("Init DB of IB finished.")
                    done = True
                    break
                else:
                    log.info("Waiting for IB init DB to finish..")
                    time.sleep(1)
            if done:
                self.delete_job(name=f"information-broker-{variant_name}-init-db")
                break

@dataclass
class VariantDeploymentDeletionClient(DeploymentClient):
    variant_name: str

    raise_exc = False
    steps = [
        "delete_ib_database_pv",
    ]

    def __step_delete_ib_database_pv(self):
        self.coreV1client.delete_persistent_volume(
            "{}-information-broker-{}-database-pv" \
                .format(self.namespace, self.variant_name)
        )

@dataclass
class MarketplaceDeploymentDeletionClient(DeploymentClient):

    raise_exc = False
    steps = [
        "delete_memo_database_pv",
        "delete_namespace"
    ]

    def __step_delete_memo_database_pv(self):
        self.coreV1client.delete_persistent_volume(
            f"{self.namespace}-memo-database-pv"
        )

    def __step_delete_namespace(self):
        self.coreV1client.delete_namespace(self.namespace)
