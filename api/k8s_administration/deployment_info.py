from dataclasses import dataclass

@dataclass
class VariantDeploymentInfo:
    variant_name: str 
    client_id: str 
    client_secret: str
    realm_admin_credentials: dict
    os_authz_config: str
    realm: str 
    aaa_url: str
    admin_username: str 
    admin_password: str 

@dataclass
class MarketplaceDeploymentInfo:
    slarc_url: str
    vcg_url: str
    itlb_url: str
    dcmcm_url: str


@dataclass
class MarketplaceDeploymentResult:
    memo_url: str
    am_url: str

@dataclass
class VariantDeploymentResult:
    ib_url: str