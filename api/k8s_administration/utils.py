from django.conf import settings
from urllib.parse import urljoin
from api.models import Region
from base64 import b64encode
import random
import string 
import re

def format_namespace(region: Region):
    return f"{region.country}-{region.region}-{region.city}".lower().replace(" ", "-")

variant_types = {
	"electricity": "energy",
	"heat": "energy",
	"flexibility": "ancillary_services", 
	"it-load": "it_load"
}

variant_forms = {
	"electricity": "electric_energy",
	"heat": "thermal_energy",
	"flexibility": "ancillary_services", 
	"it-load": "it_load"
}

def get_variant_type(variant_name):
    return variant_types[variant_name]

def get_variant_form(variant_name):
    return variant_forms[variant_name]

def encode_secret(data):
    return b64encode(data.encode('utf-8')).decode('utf-8')

def get_memo_service_url(namespace):
	return f"http://memo-service.{namespace}"

def get_am_external_url(namespace):
	return f"http://{settings.DEPLOYMENT_DEFAULTS['EXTERNAL_URL']}/catalyst/marketplace/{namespace}/am/"

def get_ib_external_url(namespace, variant_name):
	return f"http://{settings.DEPLOYMENT_DEFAULTS['EXTERNAL_URL']}/catalyst/marketplace/{namespace}/{variant_name}/information-broker/"

def get_ib_service_url(namespace, variant_name):
	return f"information-broker-{variant_name}-service.{namespace}"

def generate_password():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))

def get_ip_and_port(url):
	try:
		ip = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", url)[0]
	except IndexError:
		ip = None
	try:
		port = re.findall(":\d{1,5}" ,url)[0][1:]
	except IndexError:
		port = None
	return ip, port