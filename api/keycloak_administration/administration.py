from dataclasses import dataclass

from api.models import Region, Deployment, Flavour
import logging
from django.conf import settings
from auth.clients.keycloak.admin import Client as auth
from . auth_info import ClientAuthInfo, MarketAuthInfo
from . utils import format_realm_name, format_client_name, generate_password
from api.administration_client import AdministrationClient
import json 

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")

 

authz_scopes = ["dso-read", "dso-write",
                    "market-operator-read", "market-operator-write",
                    "aggregator-read", "aggregator-write",
                    "mbm-read", "mbm-write",
                    "mcm-read", "mcm-write"]

realm_scopes = ["known-market-actor-read", "known-market-actor-write",
                "market-participant-read", "market-participant-write"]

client_users = ["dso", "aggregator", "mbm", "mcm"]
client_roles = ["dso", "aggregator", "mbm", "mcm"]

realm_roles = ["market-participant", "memo", "operator"]
realm_users = {
    "market-participant": {
        "roles": ["known-market-actor", "market-participant"]
    },
    "operator": {
        "roles": ["known-market-actor", "operator"]
    },
    "memo": {
        "roles": ["known-market-actor", "memo"]
    }}

@dataclass
class AuthAdminClient(AdministrationClient):
    region: Region

    def __post_init__(self):
        super().__post_init__()
        self.realm_name = format_realm_name(self.region)

@dataclass
class NewMarketplaceAuthAdminClient(AuthAdminClient):

    def __post_init__(self):
        super().__post_init__()
        self.c = auth(realm="master")

    steps = [
        "create_realm",
        "create_admin_user_in_realm",
        "create_users",
        "create_realm_scopes",
        "create_realm_roles",
        "assign_roles_to_users",
        "create_role_scope_mappings",
    ]

    def deploy(self):
        super().deploy()
        return MarketAuthInfo(
            realm=self.realm_name,
        )

    def destroy(self):
        log.info("Rolling back NewVariantAuthAdminClient")
        deletion_client = MarketAuthDeletionClient(self.region)
        deletion_client.deploy()
    
    def __step_create_realm(self):
        self.c.create_realm(realm_name=self.realm_name)

    def __step_create_admin_user_in_realm(self):
        self.c.create_admin_user(realm_name=self.realm_name)

    def __step_create_users(self):
        for user in realm_users:
            self.c.create_user_in_realm(username=user,
                                password=generate_password(user),
                                first_name=user.capitalize(),
                                last_name=user.capitalize(),
                                realm_name=self.realm_name)

    def __step_create_realm_scopes(self):
        for scope in realm_scopes:
            self.c.create_client_scope_in_realm(
                scope_name=scope,
                realm_name=self.realm_name
            )

    def __step_create_realm_roles(self):
        for role in realm_roles:
            self.c.create_role_in_realm(role, realm_name=self.realm_name)

    # TODO: This needs to be fixed
    def __step_assign_roles_to_users(self):

        for user in realm_users:
            self.c.assign_realm_roles_to_user_in_realm(
                username=user,
                roles=realm_users[user]["roles"],
                realm_name=self.realm_name
            )

    def __step_create_role_scope_mappings(self):
        for scope in realm_scopes:
            for role in realm_roles:
                if role in scope:
                    self.c.create_role_scope_mappings_in_realm(scope_name=scope,
                                                        role_name=role,
                                                        realm_name=self.realm_name)	

@dataclass
class NewVariantAuthAdminClient(AuthAdminClient):
    variant: Flavour

    steps = [
        "create_client",
        "create_client_users",
        "create_auth_scopes",
        "create_client_roles",
        "assign_roles_to_users",
        "create_auth_role_policies",
        "create_permissions",
        "create_protocol_mappers",
        "get_authz_config",
    ]

    def __post_init__(self):
        super().__post_init__()
        self.client_name = format_client_name(self.variant)
        self.c = auth(realm=self.realm_name)
        self.users = [ 
            {
                "username": f"{user}-{self.variant.type}",
                "password": generate_password(user),
                "type": user
            }
            for user in client_users
        ]
        log.info("Intialized self.users as per {}".format(json.dumps(self.users, indent=4)))

        
    def deploy(self):
        super().deploy()
        credentials = {
                user['type']:{
                    "username": user['username'],
                    "password": user['password']
                }
                for user in self.users
            }
        return ClientAuthInfo(
            client_id=self.client_name,
            client_secret=self.client_secret,
            credentials=credentials,
            client_authz_settings=self.client_authz_settings
        )

    def destroy(self):
        log.info("Rolling back NewVariantAuthAdminClient")
        deletion_client = VariantAuthDeletionClient(self.region, self.variant.type)
        deletion_client.deploy()

    def __step_create_client(self):
        self.c.create_client_in_realm(self.client_name, self.realm_name)
        self.client_id = self.c.get_client_id_in_realm(self.client_name, self.realm_name)
        self.client_secret = self.c.get_client_secret_in_realm(self.client_name, self.realm_name)

    def __step_create_client_users(self):
        for user in self.users:
            self.c.create_user_in_realm(username=user['username'],
                                password=user['password'],
                                first_name=user['username'].capitalize(),
                                last_name=user['username'].capitalize(),
                                realm_name=self.realm_name)

    def __step_create_auth_scopes(self):
        for scope in authz_scopes:
            self.c.create_authz_scope_in_realm(scope_name=scope,
                                        client_name=self.client_name,
                                        realm_name=self.realm_name)

    def __step_create_client_roles(self):
        for role in client_roles:
            self.c.create_client_role_in_realm(role_name=role,
                                        client_name=self.client_name,
                                        realm_name=self.realm_name)

    def __step_assign_roles_to_users(self):
        for user in self.users:
            self.c.assign_client_roles_to_user_in_realm(username=user['username'],
                                                client_name=self.client_name,
                                                roles=[x for x in client_roles if user['type'] in x],
                                                realm_name=self.realm_name)
            self.c.assign_realm_roles_to_user_in_realm(username=user['username'],
                                                       roles=["known-market-actor"],
                                                       realm_name=self.realm_name)

    def __step_create_auth_role_policies(self):
        for client_role in client_roles:
            self.c.create_authz_role_policy_in_realm(policy_name="{}-role-policy".format(client_role),
                                                client_name=self.client_name,
                                                client_roles=[client_role],
                                                realm_roles=["known-market-actor"],
                                                realm_name=self.realm_name)

    def __step_create_permissions(self):
        for client_role in client_roles:
            relevant_client_scopes = []
            for authz_scope in authz_scopes:
                if client_role in authz_scope:
                    relevant_client_scopes.append(authz_scope)
            self.c.create_authz_client_permission_in_realm(
                permission_name="{}-permissions".format(client_role),
                client_scopes=relevant_client_scopes,
                client_policies=["{}-role-policy".format(client_role)],
                client_name=self.client_name,
                realm_name=self.realm_name
            )

    def __step_create_protocol_mappers(self):
        self.c.create_default_protocol_mappers_in_realm(realm_name=self.realm_name,
                                                        client_name=self.client_name,
                                                        client_id=self.client_id,
                                                        client_secret=self.client_secret)


    def __step_get_authz_config(self):
        self.client_authz_settings = json.dumps(self.c.get_client_authz_config_in_realm(
            client_name=self.client_name,
            realm_name=self.realm_name
        ))

@dataclass
class VariantAuthDeletionClient(AuthAdminClient):
    variant_name: str
    steps = [
        "delete_client",
        "delete_client_users"
    ]

    def __post_init__(self):
        super().__post_init__()
        self.c = auth(realm=self.realm_name)
        self.users = [ f"{user}-{self.variant_name}" for user in client_users ]

    def __step_delete_client(self):
        self.c.delete_client_in_realm(
            client_name=format_client_name(self.variant_name),
            realm_name=self.realm_name
        )

    def __step_delete_client_users(self):
        for user in self.users:
            self.c.delete_user_in_realm(username=user, realm_name=self.realm_name)

@dataclass
class MarketAuthDeletionClient(AuthAdminClient):

    steps = [
        "delete_realm"
    ]

    def __post_init__(self):
        super().__post_init__()
        self.c = auth(realm=self.realm_name)

    def __step_delete_realm(self):
        self.c.delete_realm(self.realm_name)
