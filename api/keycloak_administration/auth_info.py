from dataclasses import dataclass

@dataclass
class ClientAuthInfo:
    client_id: str
    client_secret: str
    credentials: dict
    client_authz_settings: str

@dataclass
class MarketAuthInfo:
    realm: str

    