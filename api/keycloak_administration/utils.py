import random
import string
from api.models import Region, Flavour
from api.utils import format_locality_identifier

import logging
from django.conf import settings
logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")

def format_realm_name(region: Region):
    return format_locality_identifier(region).lower().replace(" ", "-")

def format_client_name(variant):
    if type(variant) == Flavour:
        return f"catalyst-maas-{variant.type}"
    else:
        return f"catalyst-maas-{variant}"

def generate_password(user):
    ret = None
    if user in["market-participant", "operator", "dso", "memo"]:
        ret = "Marketplace2020"
    else:
        ret = ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))
    return ret