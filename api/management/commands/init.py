import logging.config
import os

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand, call_command

from api.models import Flavour, Clearing

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")

class Command(BaseCommand):
    def handle(self, *args, **options):
        log.info("Checking for a super user.")
        User = get_user_model()
        if not User.objects.filter(username=os.getenv('ADMIN_USERNAME', 'admin')).exists():
            log.info('No super user found. Creating one.')
            User.objects.create_superuser(username=os.getenv('ADMIN_USERNAME', 'admin'),
                                          email=os.getenv('ADMIN_EMAIL', 'admin@localhost.com'),
                                          password=os.getenv('ADMIN_PASSWORD', 'admin'))
        else:
            log.info("Super user exists, skipping creation")
        log.info('Done')
        log.info("Checking if initial data is there.")
        no_variants_ok = Flavour.objects.all().count() == 4
        no_clearing_ok = Clearing.objects.all().count() == 3
        if no_variants_ok and no_clearing_ok:
            log.info("DB already initialized, exiting..")
        else:
            log.info("DB not initialized, initializing..")
            call_command("loaddata", "fixtures/init_data")
            log.info("Done")
        return
