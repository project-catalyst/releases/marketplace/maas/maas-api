from uuid import uuid4

from django.db import models
from django.db.models import CASCADE


class Request(models.Model):
    REQUEST_STATUS=[
        ('received', 'Received'),
        ('started', 'Started'),
        ('completed', 'Completed')
    ]
    status = models.CharField(max_length=9, choices=REQUEST_STATUS, default='received', help_text="The status of the request")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Flavour(models.Model):
    FLAVOURS = [
        ('electricity', 'Electricity'),
        ('flexibility', 'Flexibility'),
        ('heat', 'Heat'),
        ('it-load', 'IT Load')
    ]
    type = models.CharField(max_length=20, choices=FLAVOURS, null=False, blank=False, help_text="The marketplace flavour name")

    def __str__(self):
        return self.type


class Clearing(models.Model):
    MECHANISMS = [
        ('market', 'Market Equilibrium'),
        ('selective', 'Selective'),
        ('best-fit', 'Best Fit')
    ]
    type = models.CharField(max_length=20, choices=MECHANISMS, null=False, blank=False, help_text="The marketplace clearing mechanism")

    def __str__(self):
        return self.type


class Contact(models.Model):
    username = models.CharField(unique=True, max_length=100, null=False, blank=False,help_text="The name of the contact")
    name = models.CharField(max_length=100, null=False, blank=False,help_text="The name of the contact")
    email = models.EmailField(unique=True, null=False, blank=False, help_text="The email of the contact")
    phone = models.CharField(max_length=100, null=False, blank=False, default=uuid4, help_text="The email of the contact")

    def __str__(self):
        return self.username


class Region(models.Model):
    country = models.CharField(max_length=32, null=False, blank=False, help_text="The country of the region")
    region = models.CharField(max_length=100, null=False, blank=False, help_text="The region in the country")
    city = models.CharField(max_length=100, null=False, blank=False, help_text="The city of the region")
    zip_code = models.CharField(max_length=10, null=True, blank=True, help_text="The zip code of the region")

    def __str__(self):
        return "{} - {} - {}".format(self.country, self.region, self.city)


class Deployment(models.Model):
    DEPLOYMENT_STATUS=[
        ('pending', 'Pending'),
        ('active', 'Active'),
        ('inactive', 'Inactive')
    ]
    name = models.CharField(max_length=100, unique=True, null=True, blank=True, default=uuid4, help_text="The name of the deployment")
    flavour = models.ForeignKey(Flavour, on_delete=CASCADE, help_text="The flavour of the marketplace deployment")
    clearing = models.ForeignKey(Clearing, on_delete=CASCADE, help_text="The clearing mechanism of the deployment")
    region = models.ForeignKey(Region, on_delete=CASCADE, help_text="The region of the deployment")
    operator = models.ForeignKey(Contact, null=False, blank=False, on_delete=CASCADE, help_text="The contact of the deployment")
    url = models.URLField(default=None, null=True, blank=True, help_text="The URL of the marketplace entrypoint")
    api_url = models.URLField(default=None, null=True, blank=True, help_text="The URL of the marketplace API (IB URL)")
    status = models.CharField(max_length=8, choices=DEPLOYMENT_STATUS, default='pending', help_text="The status of the deployment")
    operational = models.NullBooleanField(default=None, null=True, help_text="The operation status of the deployment")
    request = models.ForeignKey(Request, null=True, blank=True, on_delete=CASCADE, related_name='deployment', help_text='The request for the deployment')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {} ({})".format(self.flavour, self.clearing, self.region)


class Federation(models.Model):
    api_url = models.URLField(null=False, blank=False, help_text="The URL of the API")
    memo_url = models.URLField(null=False, blank=False, help_text="The URL of MEMO")
    slarc_url = models.URLField(null=False, blank=False, help_text="The URL of SLARC")
    vcg_url = models.URLField(null=False, blank=False, help_text="The URL of VCG")
    itlb_url = models.URLField(null=False, blank=False, help_text="The URL of VCG")
    aaa_url = models.URLField(null=False, blank=False, help_text="The URL of AAA")
