from rest_framework import serializers

from api.models import *


class DeploymentSerializerShort(serializers.ModelSerializer):
    class Meta:
        model = Deployment
        fields = ['id', 'name', 'flavour', 'clearing', 'status', 'request', 'created_at', 'updated_at']


class DeploymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deployment
        fields = '__all__'

class FederationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Federation
        fields = '__all__'


class FlavourSerializer (serializers.ModelSerializer):
    class Meta:
        model = Flavour
        fields = '__all__'

class ClearingSerializer (serializers.ModelSerializer):
    class Meta:
        model = Clearing
        fields = '__all__'


class RequestSerializer (serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = '__all__'


class ContactSerializer (serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'


class CredentialsSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class RefreshTokenSerializer(serializers.Serializer):
    refresh_token = serializers.CharField()


class CountriesSerializer(serializers.Serializer):
    country = serializers.CharField()


class SingleRegionSerializer(serializers.Serializer):
    region = serializers.CharField()


class CitiesSerializer(serializers.Serializer):
    city = serializers.CharField()


class RegionSerializer (serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'