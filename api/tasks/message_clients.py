import json
from abc import abstractmethod, ABCMeta
from urllib.parse import urljoin, urlparse
from api.administration_client import AdministrationClient
from dataclasses import dataclass
import requests
import logging
from django.conf import settings 
from time import sleep

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


@dataclass
class MessageClient(AdministrationClient):
    url: str
    max_retries: int
    retry_interval: int
    steps = [
        "send_message",
    ]

    def __post_init__(self):
        super().__post_init__()
        """
        p = urlparse(self.url)
        scheme = p.scheme or "http"
        path = p.netloc or p.path
        path = path.lstrip("/")
        self.url = f"{scheme}://{path}"
        """
    def get_message(self):
        pass

    def __get_result(self, response):
        return response

    def deploy(self):
        self.result = None
        super().deploy()
        return self.result

    def __step_send_message(self):
        message = self.get_message()
        for i in range(self.max_retries):
            log.info(f"Sending message, try {i}")
            res = self.send_message_raw(message)
            if res:
                log.info("Message OK")
                self.result = self.verify_response(
                    self.__get_result(
                        res
                    )
                )
                return
            sleep(self.retry_interval)
        raise Exception("Could not send message")

    def verify_response(self, response):
        if 200 <= response.status_code < 300:
            return True
        return False

@dataclass
class IBCheckClient(MessageClient):

    def __post_init__(self):
        super().__post_init__()
        log.info(f"IB url is: {self.url}")

    def deploy(self):
        try:
            return super().deploy()
        except Exception as e:
            import traceback
            log.info(f"Error messaging IB: {traceback.format_exc()}")

    def send_message_raw(self, data):
        log.info("GET URL: {}.".format(self.url))
        res = None
        try:
            res = requests.get(
                url=self.url
            )
            res.raise_for_status()
        except Exception as e:
            log.exception(e)
            return False
        return res


@dataclass
class MemoUpdateClient(MessageClient):
    type_: str
    form: str
    variant: str
    ib_credentials: dict
    mcm_credentials: dict
    mbm_credentials: dict
    locality_identifier: str
    public_k8s_ip: str


    def deploy(self):
        self.result = None
        try:
            super().deploy()
        except Exception as e:
            import traceback
            log.info(f"Error messaging MEMO: {traceback.format_exc()}")

    def get_message(self):
        return self.__format_message()

    def __post_init__(self):
        super().__post_init__()
        log.info(f"MEMO url is: {self.get_memo_update_url()}")

    def get_memo_update_url(self):
        return self.url

    def send_message_raw(self, data):
        try:
            res = requests.post(
                url=self.get_memo_update_url(),
                headers={"Content-Type": "application/json"},
                data=data,
                timeout=1800
            )
        except Exception as e:
            log.error(e)
            return False
        return res

    def get_mcm_rel_url(self, variant):
        if variant == 'electricity':
            return 'mcm_el'
        elif variant == 'flexibility':
            return 'mcm_fl'
        elif variant == 'heat':
            return 'mcm_th'
        elif variant == 'it-load':
            return 'mcm_it'
        return None

    def get_mbm_rel_url(self, variant):
        if variant == 'electricity':
            return 'mbm_el'
        elif variant == 'flexibility':
            return 'mbm_fl'
        elif variant == 'heat':
            return 'mbm_th'
        elif variant == 'it-load':
            return 'mbm_it'
        return None

    def __format_message(self):
        return json.dumps({
            "type": self.type_,
            "form": self.form,
            "components": {
                "ib": {
                    "url": "http://{}/catalyst/marketplace/{}/{}/information-broker".format(
                        self.public_k8s_ip,
                        self.locality_identifier,
                        self.variant,
                    ),
                    "username": self.ib_credentials['username'],
                    "password": self.ib_credentials['password']
                },
                "mcm": {
                    "url": "http://market-clearing-manager-{}-service:8080/{}/rest".format(self.variant, self.get_mcm_rel_url(self.variant)),
                    "username": self.mcm_credentials['username'],
                    "password": self.mcm_credentials['password']
                },
                "mbm": {
                    "url": "http://market-billing-manager-{}-service:8080/{}/rest".format(self.variant, self.get_mbm_rel_url(self.variant)),
                    "username": self.mbm_credentials['username'],
                    "password": self.mbm_credentials['password']
                }
            }
        })

        
