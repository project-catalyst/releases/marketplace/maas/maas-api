import json
import logging
import logging.config
from dataclasses import dataclass

import requests
from django.conf import settings
from api import models
from api.models import Request, Federation, Region, Deployment as DeploymentModel, Flavour
from api.keycloak_administration import *
from api.k8s_administration.administration import *
from . message_clients import MemoUpdateClient, IBCheckClient
from api.k8s_administration import VariantDeploymentInfo, get_memo_service_url, MarketplaceDeploymentInfo
from api.utils import format_locality_identifier 


logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


@dataclass
class NewVariantToExistingMarketplace(AdministrationClient):
    request_id: int

    steps = [
        "setup_auth_for_new_variant",
        "deploy_new_variant",
        "update_memo",
        "finalize_deployment"
    ]

    def __post_init__(self):
        super().__post_init__()
        self.request = Request.objects.get(id=self.request_id)
        self.request.status = "started"
        self.request.save()
        self.deployment = DeploymentModel.objects.get(request__id=self.request_id)
        self.region = self.deployment.region
        self.variant=self.deployment.flavour
        self.market_auth_info = MarketAuthInfo(
            realm=format_realm_name(self.region)
        )


    def destroy(self):
        if hasattr(self, "variant_deployment_client"):
            self.variant_deployment_client.destroy()

    def __step_setup_auth_for_new_variant(self):
        self.new_variant_auth_admin_client = NewVariantAuthAdminClient(
            region=self.deployment.region, 
            variant=self.deployment.flavour
        )
        self.client_auth_info = self.new_variant_auth_admin_client.deploy()
        
    def __step_deploy_new_variant(self):
        variant_deployment_info = VariantDeploymentInfo(
            client_id=self.client_auth_info.client_id,
            client_secret=self.client_auth_info.client_secret,
            os_authz_config=self.client_auth_info.client_authz_settings,
            variant_name=self.deployment.flavour.type,
            admin_username=generate_password(),
            admin_password=generate_password(),
            realm=self.market_auth_info.realm,
            aaa_url=Federation.objects.first().aaa_url,
            realm_admin_credentials={
                "username": settings.KEYCLOAK['admin_username'],
                "password": settings.KEYCLOAK['admin_password'],
            }
        )

        self.variant_deployment_client = VariantDeploymentClient(
            namespace=format_namespace(self.region),
            variant_deployment_info=variant_deployment_info
        )

        self.variant_deployment_result = self.variant_deployment_client.deploy()
        self.deployment.api_url = self.variant_deployment_result.ib_url
        self.deployment.save()


    def __step_update_memo(self):
        log.info("Starting MEMO update on the new Market variant deployment")
        variant_name = self.variant.type
        memo_client = MemoUpdateClient(
            type_=get_variant_type(variant_name),
            form= get_variant_form(variant_name),
            variant=variant_name,
            ib_credentials={"username":"memo", "password":"Marketplace2020"},
            mcm_credentials=self.client_auth_info.credentials['mcm'],
            mbm_credentials=self.client_auth_info.credentials['mbm'],
            locality_identifier=format_locality_identifier(self.region),
            public_k8s_ip=settings.DEPLOYMENT_DEFAULTS['EXTERNAL_URL'],
            url=get_memo_service_url(format_namespace(self.region)) + "/memo/rest/marketplaces/",
            max_retries=10,
            retry_interval=1,
        )
        # Validate that IB is ok
        while True:
            url = "http://{}/catalyst/marketplace/{}/{}/information-broker/form/".format(
                settings.DEPLOYMENT_DEFAULTS['EXTERNAL_URL'],
                format_locality_identifier(self.region),
                self.variant,
            )
            try:
                res = requests.get(url=url,
                                   timeout=1)
                res.raise_for_status()
            except Exception as e:
                log.warning("IB is not ready yet to get contacted: {}".format(e))
                time.sleep(1)
                continue
            log.info("IB is up and running, responding with {} to {}".format(res.status_code, url))
            break
        memo_client.deploy()

    def __step_finalize_deployment(self):
        self.deployment.status = "active"
        ib_checker = IBCheckClient(url=self.deployment.api_url + "/form/", max_retries=2, retry_interval=2)
        if ib_checker.deploy():
            self.deployment.operational = True
        self.request.status = "completed"
        self.deployment.save()
        self.request.save()

@dataclass
class NewVariantToNewMarketplace(NewVariantToExistingMarketplace):
    request_id: int

    steps = [
        "setup_auth_for_new_marketplace",
        "deploy_new_marketplace",
    ] + NewVariantToExistingMarketplace.steps

    def __post_init__(self):
        super().__post_init__()
        self.request = Request.objects.get(id=self.request_id)
        self.request.status = "started"
        self.request.save()
        self.deployment = DeploymentModel.objects.get(request__id=self.request_id)
        self.region = self.deployment.region
        self.namespace = format_namespace(self.region)

    def destroy(self):
        if hasattr(self, "new_marketplace_auth_admin_client"):
            self.new_marketplace_auth_admin_client.destroy()
        if hasattr(self, "new_marketplace_deployment_client"):
            self.new_marketplace_deployment_client.destroy()
        super().destroy()

    def __step_setup_auth_for_new_marketplace(self):
        self.new_marketplace_auth_admin_client = NewMarketplaceAuthAdminClient(
            region=self.deployment.region
        )
        self.new_marketplace_auth_admin_client.deploy()
    
    def __step_deploy_new_marketplace(self):
        federation = Federation.objects.first()
        self.new_marketplace_deployment_client = MarketDeploymentClient(
            namespace=self.namespace,
            marketplace_deployment_info=MarketplaceDeploymentInfo(
                slarc_url=federation.slarc_url,
                vcg_url=federation.vcg_url,
                itlb_url=federation.itlb_url,
                dcmcm_url=settings.DEPLOYMENT_DEFAULTS["DCMCM_URL"]
            )
        )
        result = self.new_marketplace_deployment_client.deploy()
        self.deployment.url = result.am_url

@dataclass
class NonLastVariantDeletionClient(AdministrationClient):
    deployment_id: int

    raise_exc = False
    steps = [
        "delete_variant_auth",
        "delete_variant_deployment",
    ]

    def __post_init__(self):
        self.deployment = DeploymentModel.objects.get(id=self.deployment_id)

    def deploy(self):
        super().deploy()
        DeploymentModel.objects.filter(id=self.deployment_id).delete()

    def __step_delete_variant_auth(self):
        auth_deletion = VariantAuthDeletionClient(
            region=self.deployment.region,
            variant_name=self.deployment.flavour.type,
        )
        auth_deletion.deploy()

    def __step_delete_variant_deployment(self):
        deployment_deletion = VariantDeploymentDeletionClient(
            namespace=format_namespace(self.deployment.region),
            variant_name=self.deployment.flavour.type
        )
        deployment_deletion.deploy()


@dataclass
class LastVariantDeletionClient(NonLastVariantDeletionClient):
    deployment_id: int

    raise_exc = False
    steps = NonLastVariantDeletionClient.steps + [
        "delete_marketplace_auth",
        "delete_marketplace_deployment"
    ]

    def __post_init__(self):
        self.deployment = DeploymentModel.objects.get(id=self.deployment_id)

    def __step_delete_marketplace_auth(self):
        auth_deletion = MarketAuthDeletionClient(
            region=self.deployment.region,
        )
        auth_deletion.deploy()

    def __step_delete_marketplace_deployment(self):
        deployment_deletion = MarketplaceDeploymentDeletionClient(
            namespace=format_namespace(self.deployment.region)
        )
        deployment_deletion.deploy()