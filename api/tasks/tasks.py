from django.conf import settings
from . task_clients import *
from maas.settings import celery_app as app

@app.task
def add_new_variant_to_existing_marketplace(request_id):
    task_client = NewVariantToExistingMarketplace(request_id)
    task_client.deploy()


@app.task
def add_variant_to_new_marketplace(request_id):
    task_client = NewVariantToNewMarketplace(request_id)
    task_client.deploy()

@app.task
def delete_last_deployment_in_region(deployment_id):
    variant_deletion = LastVariantDeletionClient(deployment_id)
    variant_deletion.deploy()

@app.task
def delete_non_last_deployment_in_region(deployment_id):
    variant_deletion = NonLastVariantDeletionClient(deployment_id)
    variant_deletion.deploy()


    
