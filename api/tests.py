from django.test import TestCase
from django.core.management import call_command
# Create your tests here.
from django.test import TestCase
from api.models import *
from api.tasks import NewVariantToNewMarketplace, NewVariantToExistingMarketplace, format_realm_name
from django.conf import settings
from auth.clients.keycloak.admin import Client as auth

class DeployVariantToNewMarket(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        call_command("loaddata", "fixtures/init_data.json", verbosity=0)
        cls.region = Region(country="Greece", region="Attica", city="Athens", zip_code="01827");
        cls.region.save()
        cls.contact = Contact(username="petros", name="Petros", email="petros@test.com", phone="523458769387");
        cls.contact.save()
        cls.requests = []
        cls.deployments = []
        cls.task_clients = []
        cls.realm_name = format_realm_name(cls.region)
        request, deployment = cls.setup_deployment_request(Flavour.objects.all()[1])
        new_marketplace_task_client = NewVariantToNewMarketplace(request.id)
        cls.task_clients.append(new_marketplace_task_client)
        new_marketplace_task_client.deploy()

    @classmethod
    def setup_deployment_request(cls, variant):
        request = Request()
        request.save()
        deployment = Deployment(
            region=cls.region,
            flavour=variant,
            clearing=Clearing.objects.first(),
            operator=cls.contact
        )
        deployment.request = request
        deployment.save()
        cls.deployments.append(deployment)
        cls.requests.append(request)
        return request, deployment

    def test_client_users_exist(self):
        c = auth(realm=self.__class__.realm_name)
        for user in ["dso", "aggregator", "mbm", "mcm"]:
            self.assertIsNotNone(
                c.get_user_id_in_realm(
                    username=f"{user}-{self.__class__.deployments[-1].flavour.type}",
                    realm_name=self.realm_name
                )
            )

    @classmethod
    def tearDownClass(cls):
        if settings.PRESERVE_TEST_DEPLOYMENTS:
            i = input("Preserve new marketplace test deployment? y/n \n")
            if i == "y":
                return
        for task_client in reversed(cls.task_clients):
            if task_client.is_complete():
                task_client.destroy()
        super().tearDownClass()



