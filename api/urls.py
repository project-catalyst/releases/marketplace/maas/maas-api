import os

from api.authorization import TokenGeneration, TokenRevocation
from api.views import *
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

# Uncomment when the Docs are fine
schema_view = get_schema_view(
    openapi.Info(
        title="CATALYST Marketplace as a Service API",
        default_version='v1',
        description="A reference implementation of the H2020 CATALYST project Marketplace as a Service API component.",
        contact=openapi.Contact(email="artemis@power-ops.com"),
        license=openapi.License(name="LGPL License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    url=None if not os.getenv('EXTERNAL_URL') else "http://{}/catalyst/maas/api/v1/docs/".format(os.getenv('EXTERNAL_URL'))
)

urlpatterns = [
    path('deployments/', csrf_exempt(DeploymentsListCreate.as_view()), name="List Deployments"),
    path('deployments/<int:pk>', csrf_exempt(DeploymentDetails.as_view()), name="Show a particular deployment"),
    path('deployments/by-request/<int:request_id>', csrf_exempt(DeploymentDetailsByRequest.as_view()), name="Show a particular deployment based on a request ID"),

    path('requests/<int:pk>', csrf_exempt(RequestDetails.as_view()), name="Show a particular request"),

    path('clearing_mechanisms/', csrf_exempt(ClearingList.as_view()), name="Show all clearing mechanisms"),

    path('flavours/', csrf_exempt(FlavourList.as_view()), name="Show all marketplace flavours"),

    path('operators/', csrf_exempt(OperatorListCreate.as_view()), name="Create an operator"),
    path('operators/<int:pk>', csrf_exempt(OperatorDetails.as_view()), name="Show details of an operator"),

    path('federation/', csrf_exempt(FederationList.as_view()), name="Show the federation details"),
    path('federation/<int:pk>', csrf_exempt(FederationDetails.as_view()), name="Show the federation details"),

    path('token/', csrf_exempt(TokenGeneration.as_view()), name="Get authorization token"),
    path('token/logout', csrf_exempt(TokenRevocation.as_view()), name="Revoke a token"),

    path('regions/', csrf_exempt(RegionListCreate.as_view()), name="Show regions and create new ones"),
    path('regions/<int:pk>', csrf_exempt(RegionDetails.as_view()), name="Manage regions"),
    path('regions/countries/', csrf_exempt(Countries.as_view()), name="Show regions filtered on country"),
    path('regions/countries/<str:country>/regions/', csrf_exempt(RegionsbyCountry.as_view()),
         name="Show regions filtered on country"),
    path('regions/countries/<str:country>/regions/<str:region>/cities/', csrf_exempt(CitiesByRegion.as_view()),
         name="Show regions filtered on country"),
    path('regions/repr/<str:country>/<str:region>/<str:city>/', csrf_exempt(RegionByDistinctValues.as_view()),
         name="Get a region object based on the country, region and city details."),

    path('docs/', csrf_exempt(schema_view.with_ui('swagger', cache_timeout=0)), name='schema-swagger-ui'),
]
