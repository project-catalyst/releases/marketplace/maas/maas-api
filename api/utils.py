from api.models import Region

def format_locality_identifier(region):
    return f"{region.country}-{region.region}-{region.city}".lower().replace(" ", "-")