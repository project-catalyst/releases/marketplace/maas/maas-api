from django.conf import settings

import logging
import logging.config

from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from api.models import *
from api.paginators import BigResultsSetPagination
from api.serializers import *
from api.tasks import add_new_variant_to_existing_marketplace, add_variant_to_new_marketplace, delete_last_deployment_in_region, delete_non_last_deployment_in_region

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class DeploymentsListCreate(generics.ListCreateAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
        'POST': 'maas-api-write'
    }
    queryset = Deployment.objects.all()
    pagination_class = PageNumberPagination

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return DeploymentSerializerShort
        elif self.request.method == 'POST':
            return DeploymentSerializer
        else:
            return DeploymentSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset().order_by('-created_at')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(data=serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            content =  {"msg": "Error decoding the request: {}".format(serializer.errors)}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        # save started request
        _request = Request.objects.create()

        region, variant = serializer.validated_data.get("region"), serializer.validated_data.get("flavour")

        # create new market if needed; add variant to market
        existing_deployments_qs = Deployment.objects.filter(region=region)
        if existing_deployments_qs.exists():
            if existing_deployments_qs.filter(flavour=variant).exists():
                content =  {"msg": "Flavour already exists in marketplace"}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
            else:
                # save pending deployment
                deployment = serializer.save()
                deployment.request = _request; deployment.save()
                # add variant to existing marketplace
                add_new_variant_to_existing_marketplace.apply_async((_request.id,))
        else:
            # save pending deployment
            deployment = serializer.save()
            deployment.request = _request; deployment.save()
            # create new marketplace and add variant
            add_variant_to_new_marketplace.apply_async((_request.id,))

        ret = RequestSerializer(instance=_request, many=False)
        return Response(ret.data, status=status.HTTP_200_OK)



class DeploymentDetails(generics.RetrieveDestroyAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
        'PUT': 'maas-api-write',
        'DELETE': 'maas-api-write'
    }
    queryset = Deployment.objects.all()
    serializer_class = DeploymentSerializer

    def delete(self, request, pk):
        try:
            deployment = Deployment.objects.get(id=pk)
        except Deployment.DoesNotExist:
            content = {"msg": "Deployment not found"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)
        existing_deployments_qs = Deployment.objects.filter(region=deployment.region)
        if existing_deployments_qs.count() == 1:
            delete_last_deployment_in_region.apply_async((pk,))
        else:
            delete_non_last_deployment_in_region.apply_async((pk,))
        return Response(status=status.HTTP_204_NO_CONTENT)

class DeploymentDetailsByRequest(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Deployment.objects.all()
    serializer_class = DeploymentSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(request=int(kwargs['request_id']))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(data=serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)




class FederationList(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Federation.objects.all()
    serializer_class = FederationSerializer


class FederationDetails(generics.UpdateAPIView):
    keycloak_scopes = {
        'PUT': 'maas-api-write'
    }
    queryset = Federation.objects.all()
    serializer_class = FederationSerializer


class FlavourList(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Flavour.objects.all()
    serializer_class = FlavourSerializer


class ClearingList(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Clearing.objects.all()
    serializer_class = ClearingSerializer


class RequestDetails(generics.RetrieveAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Request.objects.all()
    serializer_class = RequestSerializer


class OperatorListCreate(generics.ListCreateAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
        'POST': 'maas-api-write'
    }
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    pagination_class = BigResultsSetPagination


class OperatorDetails(generics.RetrieveAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read'
    }
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class RegionListCreate(generics.ListCreateAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
        'POST': 'maas-api-write'
    }
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    pagination_class = BigResultsSetPagination

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if self.get_queryset().filter(country=serializer.validated_data['country'].capitalize(),
                                          region=serializer.validated_data['region'].capitalize(),
                                          city=serializer.validated_data['city'].capitalize()).count() > 0:
                return Response(data={"details": "This region already exists"},
                                status=status.HTTP_409_CONFLICT)
            serializer.validated_data['country'] = serializer.validated_data['country'].capitalize()
            serializer.validated_data['region'] = serializer.validated_data['region'].capitalize()
            serializer.validated_data['city'] = serializer.validated_data['city'].capitalize()
            region = serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            content = {"msg": "Error decoding the request: {}".format(serializer.errors)}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class RegionDetails(generics.RetrieveUpdateDestroyAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
        'PUT': 'maas-api-write',
        'DELETE': 'maas-api-write'
    }
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class Countries(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
    }
    queryset = Region.objects.all()
    serializer_class = CountriesSerializer
    pagination_class = BigResultsSetPagination

    def list(self, request, *args, **kwargs):
        q = self.get_queryset().values('country').distinct()
        serializer = self.get_serializer(q, many=True)
        return Response(serializer.data)


class RegionsbyCountry(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
    }
    queryset = Region.objects.all()
    serializer_class = SingleRegionSerializer
    pagination_class = BigResultsSetPagination

    def list(self, request, *args, **kwargs):
        q = self.get_queryset().filter(country=kwargs['country']).values('region').distinct()
        serializer = self.get_serializer(q, many=True)
        return Response(serializer.data)


class CitiesByRegion(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
    }
    queryset = Region.objects.all()
    serializer_class = CitiesSerializer
    pagination_class = BigResultsSetPagination

    def list(self, request, *args, **kwargs):
        q = self.get_queryset().filter(country=kwargs['country'], region=kwargs['region']).values('city').distinct()
        serializer = self.get_serializer(q, many=True)
        return Response(serializer.data)


class RegionByDistinctValues(generics.ListAPIView):
    keycloak_scopes = {
        'GET': 'maas-api-read',
    }
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    pagination_class = BigResultsSetPagination

    def list(self, request, *args, **kwargs):
        q = self.get_queryset().filter(country=kwargs['country'], region=kwargs['region'], city=kwargs['city'])
        if q.count() > 1:
            return Response(data={"details": "More than one regions found matching the given criteria, "
                                             "please contact the administrator"},
                            status=500)
        serializer = self.get_serializer(q.first(), many=False)
        return Response(serializer.data)
