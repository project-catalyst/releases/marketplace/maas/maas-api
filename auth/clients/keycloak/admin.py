import json
import logging
import logging.config
from dataclasses import dataclass

import requests
from django.conf import settings
from keycloak import KeycloakAdmin, URL_ADMIN_CLIENT_SCOPES, raise_error_from_response, KeycloakGetError, \
    URL_ADMIN_USERS, URL_ADMIN_USER_CLIENT_ROLES, URL_ADMIN_CLIENTS, URL_ADMIN_CLIENT_ROLES, URL_ADMIN_REALM_ROLES, \
    URL_ADMIN_CLIENT_SECRETS, URL_ADMIN_CLIENT_AUTHZ_SETTINGS, URL_ADMIN_USER_REALM_ROLES, KeycloakOpenID
from auth.clients.keycloak.urls.admin import URL_ADMIN_CREATE_AUTHZ_SCOPE, URL_ADMIN_CREATE_AUTHZ_POLICY_ROLE, \
    URL_ADMIN_CREATE_AUTHZ_PERMISSION, URL_ADMIN_AUTHZ_POLICY, URL_ADMIN_SCOPE_MAPPINGS, URL_ADMIN_DELETE_REALM, \
    URL_ADMIN_OPENID_TOKEN, URL_ADMIN_DELETE_CLIENT, URL_ADMIN_DELETE_USER, URL_ADMIN_CREATE_PROTOCOL_MAPPERS

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


@dataclass
class Client(object):
    client: KeycloakAdmin = None
    realm: str = None

    def __init__(self,
                 realm=None,
                 server_url=None,
                 username=None,
                 password=None):
        self.realm = realm
        self.server_url = settings.KEYCLOAK['url'] if not server_url else server_url
        self.client = KeycloakAdmin(server_url=self.server_url,
                                    username=settings.KEYCLOAK['admin_username'] if not username else username,
                                    password=settings.KEYCLOAK['admin_password'] if not password else password,
                                    realm_name=settings.KEYCLOAK['marketplace_realm_name'] if not realm else realm,
                                    verify=False)

    def get_client(self):
        return self.client

    # Custom operations helper functions
    def post_to_keycloak(self, url, payload):
        data_raw = self.client.raw_post(url,
                                        data=json.dumps(payload))
        if data_raw.status_code not in [200, 201, 204]:
            log.error("Error while POSTing to {}".format(url))
            log.error("Status code: {}".format(data_raw.status_code))
            log.error("Content:     {}".format(data_raw.content.decode("utf-8")))
            raise Exception("Post to Keycloak failed")
        if 'location' in data_raw.headers:
            _last_slash_idx = data_raw.headers['Location'].rindex('/')
            return data_raw.headers['Location'][_last_slash_idx + 1:]
        else:
            return data_raw.content.decode("utf-8")

    def get_from_keycloak(self, url):
        data_raw = self.client.raw_get(url)
        return raise_error_from_response(data_raw, KeycloakGetError)

    def create_realm(self, realm_name):
        payload = {
            "accessCodeLifespan": 60,
            "accessCodeLifespanLogin": 1800,
            "accessCodeLifespanUserAction": 300,
            "accessTokenLifespan": 3600,
            "accessTokenLifespanForImplicitFlow": 900,
            "actionTokenGeneratedByAdminLifespan": 43200,
            "actionTokenGeneratedByUserLifespan": 300,
            "adminEventsDetailsEnabled": False,
            "adminEventsEnabled": False,
            "attributes": {
                "_browser_header.contentSecurityPolicy": "frame-src 'self'; frame-ancestors 'self'; object-src 'none';",
                "_browser_header.contentSecurityPolicyReportOnly": "",
                "_browser_header.strictTransportSecurity": "max-age=31536000; includeSubDomains",
                "_browser_header.xContentTypeOptions": "nosniff",
                "_browser_header.xFrameOptions": "SAMEORIGIN",
                "_browser_header.xRobotsTag": "none",
                "_browser_header.xXSSProtection": "1; mode=block",
                "actionTokenGeneratedByAdminLifespan": "43200",
                "actionTokenGeneratedByUserLifespan": "300",
                "bruteForceProtected": "false",
                "failureFactor": "30",
                "maxDeltaTimeSeconds": "43200",
                "maxFailureWaitSeconds": "900",
                "minimumQuickLoginWaitSeconds": "60",
                "offlineSessionMaxLifespan": "5184000",
                "offlineSessionMaxLifespanEnabled": "false",
                "permanentLockout": "false",
                "quickLoginCheckMilliSeconds": "1000",
                "waitIncrementSeconds": "60",
                "webAuthnPolicyAttestationConveyancePreference": "not specified",
                "webAuthnPolicyAttestationConveyancePreferencePasswordless": "not specified",
                "webAuthnPolicyAuthenticatorAttachment": "not specified",
                "webAuthnPolicyAuthenticatorAttachmentPasswordless": "not specified",
                "webAuthnPolicyAvoidSameAuthenticatorRegister": "false",
                "webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless": "false",
                "webAuthnPolicyCreateTimeout": "0",
                "webAuthnPolicyCreateTimeoutPasswordless": "0",
                "webAuthnPolicyRequireResidentKey": "not specified",
                "webAuthnPolicyRequireResidentKeyPasswordless": "not specified",
                "webAuthnPolicyRpEntityName": "keycloak",
                "webAuthnPolicyRpEntityNamePasswordless": "keycloak",
                "webAuthnPolicyRpId": "",
                "webAuthnPolicyRpIdPasswordless": "",
                "webAuthnPolicySignatureAlgorithms": "ES256",
                "webAuthnPolicySignatureAlgorithmsPasswordless": "ES256",
                "webAuthnPolicyUserVerificationRequirement": "not specified",
                "webAuthnPolicyUserVerificationRequirementPasswordless": "not specified"
            },
            "browserFlow": "browser",
            "browserSecurityHeaders": {
                "contentSecurityPolicy": "frame-src 'self'; frame-ancestors 'self'; object-src 'none';",
                "contentSecurityPolicyReportOnly": "",
                "strictTransportSecurity": "max-age=31536000; includeSubDomains",
                "xContentTypeOptions": "nosniff",
                "xFrameOptions": "SAMEORIGIN",
                "xRobotsTag": "none",
                "xXSSProtection": "1; mode=block"
            },
            "bruteForceProtected": False,
            "clientAuthenticationFlow": "clients",
            "defaultRoles": [
                "known-market-actor"
            ],
            "directGrantFlow": "direct grant",
            "dockerAuthenticationFlow": "docker auth",
            "duplicateEmailsAllowed": False,
            "editUsernameAllowed": False,
            "enabled": True,
            "enabledEventTypes": [],
            "eventsEnabled": False,
            "eventsListeners": [
                "jboss-logging"
            ],
            "failureFactor": 30,
            "id": realm_name,
            "internationalizationEnabled": False,
            "loginWithEmailAllowed": True,
            "maxDeltaTimeSeconds": 43200,
            "maxFailureWaitSeconds": 900,
            "minimumQuickLoginWaitSeconds": 60,
            "notBefore": 0,
            "offlineSessionIdleTimeout": 2592000,
            "offlineSessionMaxLifespan": 5184000,
            "offlineSessionMaxLifespanEnabled": False,
            "otpPolicyAlgorithm": "HmacSHA1",
            "otpPolicyDigits": 6,
            "otpPolicyInitialCounter": 0,
            "otpPolicyLookAheadWindow": 1,
            "otpPolicyPeriod": 30,
            "otpPolicyType": "totp",
            "otpSupportedApplications": [
                "FreeOTP",
                "Google Authenticator"
            ],
            "permanentLockout": False,
            "quickLoginCheckMilliSeconds": 1000,
            "realm": realm_name,
            "refreshTokenMaxReuse": 0,
            "registrationAllowed": False,
            "registrationEmailAsUsername": False,
            "registrationFlow": "registration",
            "rememberMe": False,
            "requiredCredentials": [
                "password"
            ],
            "resetCredentialsFlow": "reset credentials",
            "resetPasswordAllowed": False,
            "revokeRefreshToken": False,
            "smtpServer": {},
            "sslRequired": "external",
            "ssoSessionIdleTimeout": 7200,
            "ssoSessionIdleTimeoutRememberMe": 0,
            "ssoSessionMaxLifespan": 36000,
            "ssoSessionMaxLifespanRememberMe": 0,
            "supportedLocales": [],
            "userManagedAccessAllowed": False,
            "verifyEmail": False,
            "waitIncrementSeconds": 60,
            "webAuthnPolicyAcceptableAaguids": [],
            "webAuthnPolicyAttestationConveyancePreference": "not specified",
            "webAuthnPolicyAuthenticatorAttachment": "not specified",
            "webAuthnPolicyAvoidSameAuthenticatorRegister": False,
            "webAuthnPolicyCreateTimeout": 0,
            "webAuthnPolicyPasswordlessAcceptableAaguids": [],
            "webAuthnPolicyPasswordlessAttestationConveyancePreference": "not specified",
            "webAuthnPolicyPasswordlessAuthenticatorAttachment": "not specified",
            "webAuthnPolicyPasswordlessAvoidSameAuthenticatorRegister": False,
            "webAuthnPolicyPasswordlessCreateTimeout": 0,
            "webAuthnPolicyPasswordlessRequireResidentKey": "not specified",
            "webAuthnPolicyPasswordlessRpEntityName": "keycloak",
            "webAuthnPolicyPasswordlessRpId": "",
            "webAuthnPolicyPasswordlessSignatureAlgorithms": [
                "ES256"
            ],
            "webAuthnPolicyPasswordlessUserVerificationRequirement": "not specified",
            "webAuthnPolicyRequireResidentKey": "not specified",
            "webAuthnPolicyRpEntityName": "keycloak",
            "webAuthnPolicyRpId": "",
            "webAuthnPolicySignatureAlgorithms": [
                "ES256"
            ],
            "webAuthnPolicyUserVerificationRequirement": "not specified"
        }
        self.client.create_realm(payload=payload,
                                 skip_exists=True)

    def get_admin_token_in_realm(self, realm_name):
        keycloak_url = settings.KEYCLOAK["url"].rstrip("/")
        admin_username = settings.KEYCLOAK["admin_username"]
        admin_password = settings.KEYCLOAK["admin_password"]
        auth_res = requests.post(
            url=keycloak_url + URL_ADMIN_OPENID_TOKEN.format(realm_name=realm_name),
            data={
                "username": admin_username,
                "password": admin_password,
                "grant_type": "password",
                "client_id": "admin-cli"
            }
        )
        token = auth_res.json()['access_token']
        return token

    def delete_realm(self, realm_name):
        # custom implementation since client does not provide delete operation
        keycloak_url = self.server_url.rstrip("/")
        token = self.get_admin_token_in_realm(realm_name)
        res = requests.delete(
            url=keycloak_url + URL_ADMIN_DELETE_REALM.format(realm_name=realm_name),
            headers={
                "Authorization": f"Bearer {token}"
            }
        )

    def delete_client_in_realm(self, realm_name, client_name):
        keycloak_url = self.server_url.rstrip("/")
        token = self.get_admin_token_in_realm(realm_name)
        client_id = self.get_client_id_in_realm(client_name, realm_name)
        res = requests.delete(
            url=keycloak_url + URL_ADMIN_DELETE_CLIENT.format(realm_name=realm_name, client_id=client_id),
            headers={
                "Authorization": f"Bearer {token}"
            }
        )

    def delete_user_in_realm(self, realm_name, username):
        keycloak_url = self.server_url.rstrip("/")
        token = self.get_admin_token_in_realm(realm_name)
        user_id = self.get_user_id_in_realm(username, realm_name)
        res = requests.delete(
            url=keycloak_url + URL_ADMIN_DELETE_USER.format(realm_name=realm_name, user_id=user_id),
            headers={
                "Authorization": f"Bearer {token}"
            }
        )

    # Client-operations
    def create_client(self, client_name):
        """
        Creates a client application with default settings
        :param client_name: The name of the client
        :return: The client application
        """
        return self.create_client_in_realm(client_name=client_name,
                                           realm_name=self.realm)

    def get_client_secret(self, client_name):
        """
        Gets the secret of a client app
        :param client_name: The name of the client application
        :return: The client application secret
        """
        return self.get_client_secret_in_realm(client_name=client_name,
                                               realm_name=self.realm)

    def get_client_authz_config(self, client_name):
        """
        Gets the authentication config of a client app
        :param client_name: The name of the client app
        :return: The authentication configuration of the client app
        """
        return self.get_client_authz_config_in_realm(client_name=client_name,
                                                     realm_name=self.realm)

    # User operations
    def create_admin_user(self,
                          realm_name=None,
                          username=settings.KEYCLOAK['admin_username'],
                          password=settings.KEYCLOAK['admin_password'],
                          first_name="Admin",
                          last_name="Admin"):
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        payload = {
            "attributes": {},
            "credentials": [
                {
                    "type": "password",
                    "value": password
                }
            ],
            "enabled": True,
            "firstName": first_name,
            "lastName": last_name,
            "realmRoles": [
                "user_default"
            ],
            "emailVerified": True,
            "username": username
        }
        ret = self.post_to_keycloak(url=URL_ADMIN_USERS.format(**params_path),
                                    payload=payload)

        admin_roles = self.get_client_roles_in_realm(client_name="realm-management", realm_name=realm_name)
        payload = admin_roles

        user_id = self.get_user_id_in_realm(username, realm_name)
        client_id = self.get_client_id_in_realm('realm-management', realm_name)

        params_path = {"realm-name": realm_name if realm_name else self.realm, "id": user_id, "client-id": client_id}
        ret = self.post_to_keycloak(url=URL_ADMIN_USER_CLIENT_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def create_user(self, username, password, first_name, last_name):
        """
        Creates a user in the realm.
        :param username:
        :param password:
        :param first_name:
        :param last_name:
        :return:
        """
        payload = {
            "attributes": {},
            "credentials": [
                {
                    "type": "password",
                    "value": password
                }
            ],
            "enabled": True,
            "firstName": first_name,
            "lastName": last_name,
            "realmRoles": [
                "user_default"
            ],
            "emailVerified": True,
            "username": username
        }
        ret = self.client.create_user(payload=payload)
        return ret

    # Role operations
    def get_client_roles(self, client_name):
        """
        Gets the roles of a client application
        :param client_name: The client name to look up for
        :return: The list of client roles
        """
        return self.get_client_roles_in_realm(client_name=client_name,
                                              realm_name=self.realm)

    def get_realm_roles(self):
        return self.get_realm_roles_in_realm(realm_name=self.realm)

    def create_client_role(self, role_name, client_name):
        """
        Creates a role in an app client context
        :param role_name: The name of the role
        :param client_name: The name of app client
        :return: Nothing
        """
        return self.create_client_role_in_realm(role_name=role_name,
                                                client_name=client_name,
                                                realm_name=self.realm)

    def assign_client_roles_to_user(self, username, client_name, roles):
        """
        Assigns roles to a user
        :param username: The username of interest
        :param client_name: The app client name
        :param roles: The list of roles to add
        :return: Nothing
        """
        return self.assign_client_roles_to_user_in_realm(username=username,
                                                         client_name=client_name,
                                                         roles=roles,
                                                         realm_name=self.realm)

    def assign_realm_roles_to_user(self, username, client_name, roles):
        """
        Assigns realm roles to a user
        :param username: The username of the user
        :param roles: The list of roles to assign (List[str])
        :return: Nothing
        """
        return self.assign_realm_roles_to_user_in_realm(username=username,
                                                        client_name=client_name,
                                                        roles=roles,
                                                        realm_name=self.realm)

    # Scope operations
    def get_client_scopes(self):
        """
        Gets the realm client scopes
        :return: The list of client scopes
        """
        scopes = self.client.get_client_scopes()
        return scopes

    def create_client_scope(self, scope_name):
        """
        Creates a client scope
        :param scope_name: The scope name
        :return: The ID of the scope
        """
        return self.create_client_scope_in_realm(scope_name=scope_name,
                                                 realm_name=self.realm)

    def create_authz_scope(self, scope_name, client_name):
        """
        Creates an authorization scope
        :param scope_name: The scope name
        :param client_name: The name of the client app
        :return:
        """
        return self.create_authz_scope_in_realm(scope_name=scope_name,
                                                client_name=client_name,
                                                realm_name=self.realm)

    def get_authz_scopes(self, client_name):
        """
        Gets the authorization scopes of a client
        :param client_name: The client name
        :return: List[ScopeRepresentation] The list of authorization scopes
        """
        return self.get_authz_scopes_in_realm(client_name=client_name,
                                              realm_name=self.realm)

    def create_authz_role_policy(self,
                                 policy_name,
                                 client_name,
                                 client_roles,
                                 realm_roles):
        """
        Creates an authorization role policy for a particular client
        :param policy_name: The policy name
        :param client_name: The client name
        :param client_roles: List[str] The list of client roles of interest
        :param realm_roles: List[str] The list of realm roles of interest
        :return: The uuid of the role policy
        """
        return self.create_authz_role_policy_in_realm(policy_name=policy_name,
                                                      client_name=client_name,
                                                      client_roles=client_roles,
                                                      realm_roles=realm_roles,
                                                      realm_name=self.realm)

    def get_authz_role_policies(self, client_name):
        """
        Gets the authorization role policies of a client
        :param client_name: The client name
        :return: List[PolicyRepresentation] The list of authorization role policies
        """
        return self.get_authz_role_policies_in_realm(client_name=client_name,
                                                     realm_name=self.realm)

    def create_authz_client_permission(self, permission_name, client_scopes, client_policies, client_name):
        """
        Creates an authorization permission of a client
        :param permission_name: The permission name
        :param client_scopes: List[str] The list of client scopes (name)
        :param client_policies: List[str] The list of client policies (name)
        :param client_name: The client name
        :return:
        """
        return self.create_authz_client_permission_in_realm(permission_name=permission_name,
                                                            client_scopes=client_scopes,
                                                            client_policies=client_policies,
                                                            client_name=client_name,
                                                            realm_name=self.realm)

    def create_role_in_realm(self, role_name, realm_name=None):
        """
        Creates a role in a realm context
        :param role_name: The role name
        :param realm: The realm of interest
        :return: None
        """
        realm_name = self.realm if not realm_name else realm_name
        payload = {'name': role_name,
                   'composite': True,
                   'clientRole': False,
                   'containerId': realm_name}
        params_path = {"realm-name": realm_name}
        ret = self.post_to_keycloak(URL_ADMIN_REALM_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def get_client_id_in_realm(self, client_name, realm_name):
        """
        Gets the id of a client in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: The client id
        """
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        clients = self.get_from_keycloak(URL_ADMIN_CLIENTS.format(**params_path))
        for client in clients:
            if client_name == client.get('name') or client_name == client.get('clientId'):
                return client["id"]
        return None

    def get_user_id_in_realm(self, username, realm_name):
        """
        Gets the user id in a realm
        :param username: The username of the user
        :param realm_name: The realm name
        :return: The user id
        """
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        users = self.fetch_all(URL_ADMIN_USERS.format(**params_path), {"search": username})
        return next((user["id"] for user in users if user["username"] == username), None)

    def get_client_roles_in_realm(self, client_name, realm_name):
        """
        Gets the client roles in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: List[RoleRepresentation] The list of client roles
        """
        client_id = self.get_client_id_in_realm(client_name, realm_name)
        params_path = {"realm-name": realm_name if realm_name else self.realm, "id": client_id}
        data_raw = self.get_from_keycloak(URL_ADMIN_CLIENT_ROLES.format(**params_path))
        return data_raw

    def fetch_all(self, url, query=None):
        '''Wrapper function to paginate GET requests

        :param url: The url on which the query is executed
        :param query: Existing query parameters (optional)

        :return: Combined results of paginated queries
        '''
        results = []

        # initalize query if it was called with None
        if not query:
            query = {}
        page = 0
        query['max'] = 100

        # fetch until we can
        while True:
            query['first'] = page * 100
            partial_results = raise_error_from_response(
                self.client.raw_get(url, **query),
                KeycloakGetError)
            if not partial_results:
                break
            results.extend(partial_results)
            page += 1
        return results

    def create_user_in_realm(self, username, password, first_name, last_name, realm_name):
        """
        Creates a user in a realm
        :param username: The user name
        :param password: The user password
        :param first_name: The user first name
        :param last_name: The user last name
        :param realm_name: The realm name
        :return:
        """
        params_path = {"realm-name": realm_name if realm_name else self.realm}
        payload = {
            "attributes": {},
            "credentials": [
                {
                    "type": "password",
                    "value": password
                }
            ],
            "enabled": True,
            "firstName": first_name,
            "lastName": last_name,
            "realmRoles": [
                "user_default"
            ],
            "emailVerified": True,
            "username": username
        }
        ret = self.post_to_keycloak(url=URL_ADMIN_USERS.format(**params_path),
                                    payload=payload)
        return ret

    def create_authz_scope_in_realm(self, scope_name, client_name, realm_name):
        """
        Creates an authorization scope in a realm
        :param scope_name: The scope name
        :param client_name: The client name
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        params_path = {"client-id": client_id, "realm-name": realm_name}
        payload = {
            "name": scope_name
        }
        ret = self.post_to_keycloak(url=URL_ADMIN_CREATE_AUTHZ_SCOPE.format(**params_path),
                                    payload=payload)
        return json.loads(ret)

    def create_authz_role_policy_in_realm(self, policy_name, client_name, client_roles, realm_roles, realm_name):
        """
        Creates an Authorization role policy in a realm
        :param policy_name: The policy name
        :param client_name: The client name
        :param client_roles: List[str] The ist of client roles
        :param realm_roles: List[str] The list of realm roles
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        params_path = {"client-id": client_id, "realm-name": realm_name}
        # Get the role uuids
        _client_roles = [x['id'] for x in self.get_client_roles_in_realm(client_name=client_name, realm_name=realm_name)
                         if
                         x['name'] in client_roles] if client_roles else []
        _realm_roles = [x['id'] for x in self.get_realm_roles_in_realm(realm_name=realm_name) if
                        x['name'] in realm_roles] if realm_roles else []
        merged_roles = _client_roles + _realm_roles
        roles = [{"id": x, "required": True} for x in merged_roles]
        payload = {"decisionStrategy": "UNANIMOUS",
                   "logic": "POSITIVE",
                   "name": policy_name,
                   "roles": roles,
                   "type": "role"}
        ret = self.post_to_keycloak(url=URL_ADMIN_CREATE_AUTHZ_POLICY_ROLE.format(**params_path),
                                    payload=payload)
        return json.loads(ret)

    def get_authz_scopes_in_realm(self, client_name, realm_name):
        """
        Gets the Authorization scopes of a client in a realm
        :param client_name: The client name
        :param realm_name: The role name
        :return: List[ScopeRepresentation] The list of authorization scopes of the client
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        params_path = {"client-id": client_id, "realm-name": realm_name}
        ret = self.get_from_keycloak(url=URL_ADMIN_CREATE_AUTHZ_SCOPE.format(**params_path))
        return ret

    def create_authz_client_permission_in_realm(self, permission_name, client_scopes, client_policies, client_name,
                                                realm_name):
        """
        Creates Authentication permissions in a client in a realm
        :param permission_name: The permission name
        :param client_scopes: List[str] The desired client scopes
        :param client_policies: List[str] The list of client policies
        :param client_name: The client name
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name, realm_name=realm_name)
        params_path = {"client-id": client_id, "realm-name": realm_name}
        scopes = [x['id'] for x in self.get_authz_scopes_in_realm(client_name, realm_name) if
                  x['name'] in client_scopes]
        if len(scopes) < 1:
            return
        policies = [x['id'] for x in self.get_authz_role_policies_in_realm(client_name, realm_name) if
                    x['name'] in client_policies]
        payload = {"type": "scope",
                   "logic": "POSITIVE",
                   "decisionStrategy": "UNANIMOUS",
                   "name": permission_name,
                   "scopes": scopes,
                   "policies": policies
                   }
        ret = self.post_to_keycloak(url=URL_ADMIN_CREATE_AUTHZ_PERMISSION.format(**params_path),
                                    payload=payload)
        return json.loads(ret)['id']

    def get_realm_roles_in_realm(self, realm_name):
        """
        Gets the roles in a realm
        :param realm_name: The realm name
        :return: List[RoleRepresentation] The list of realm roles
        """
        realm_name = realm_name if realm_name else self.realm
        params_path = {"realm-name": realm_name}
        ret = self.get_from_keycloak(URL_ADMIN_REALM_ROLES.format(**params_path))
        return ret

    def create_client_role_in_realm(self, role_name, client_name, realm_name):
        """
        Creates a default client role in a realm
        :param role_name: The role name
        :param client_name: The client name
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name, realm_name=realm_name)
        params_path = {"realm-name": realm_name, "id": client_id}
        payload = {
            "attributes": {},
            "clientRole": True,
            "composite": False,
            "name": role_name,
            "containerId": client_id
        }
        ret = self.post_to_keycloak(URL_ADMIN_CLIENT_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def assign_client_roles_to_user_in_realm(self, username, client_name, roles, realm_name):
        """
        Assigns client roles to a user in a realm
        :param username: The username of the user
        :param client_name: The client name
        :param roles: List[str] The list of roles to assign
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        user_id = self.get_user_id_in_realm(username=username, realm_name=realm_name)
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        all_roles = self.get_client_roles_in_realm(client_name=client_name,
                                                   realm_name=realm_name)
        roles_repr = [x for x in all_roles if x['name'] in roles]
        payload = roles_repr
        params_path = {"realm-name": realm_name, "id": user_id, "client-id": client_id}
        ret = self.post_to_keycloak(URL_ADMIN_USER_CLIENT_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def create_client_in_realm(self, client_name, realm_name):
        """
        Creates a client in a realm (with default options)
        :param client_name: The client name
        :param realm_name: The realm name
        :return:
        """
        realm_name = realm_name if realm_name else self.realm
        payload = {
            "access": {
                "configure": True,
                "manage": True,
                "view": True
            },
            "alwaysDisplayInConsole": False,
            "attributes": {
                "display.on.consent.screen": "False",
                "exclude.session.state.from.auth.response": "False",
                "saml.assertion.signature": "False",
                "saml.authnstatement": "False",
                "saml.client.signature": "False",
                "saml.encrypt": "False",
                "saml.force.post.binding": "False",
                "saml.multivalued.roles": "False",
                "saml.onetimeuse.condition": "False",
                "saml.server.signature": "False",
                "saml.server.signature.keyinfo.ext": "False",
                "saml_force_name_id_format": "False",
                "tls.client.certificate.bound.access.tokens": "False"
            },
            "authenticationFlowBindingOverrides": {},
            "authorizationServicesEnabled": True,
            "bearerOnly": False,
            "clientAuthenticatorType": "client-secret",
            "clientId": client_name,
            "consentRequired": False,
            "defaultClientScopes": [
                "web-origins",
                "role_list",
                "roles",
                "profile",
                "email"
            ],
            "directAccessGrantsEnabled": True,
            "enabled": True,
            "frontchannelLogout": False,
            "fullScopeAllowed": True,
            "implicitFlowEnabled": False,
            "nodeReRegistrationTimeout": -1,
            "notBefore": 0,
            "optionalClientScopes": [
                "address",
                "phone",
                "offline_access",
                "microprofile-jwt"
            ],
            "protocol": "openid-connect",
            "protocolMappers": [
                {
                    "config": {
                        "access.token.claim": "True",
                        "claim.name": "clientAddress",
                        "id.token.claim": "True",
                        "jsonType.label": "String",
                        "user.session.note": "clientAddress"
                    },
                    "consentRequired": False,
                    "name": "Client IP Address",
                    "protocol": "openid-connect",
                    "protocolMapper": "oidc-usersessionmodel-note-mapper"
                },
                {
                    "config": {
                        "access.token.claim": "true",
                        "claim.name": "realm_roles",
                        "id.token.claim": "true",
                        "jsonType.label": "String",
                        "multivalued": "true",
                        "userinfo.token.claim": "true"
                    },
                    "consentRequired": False,
                    "name": "realm_roles",
                    "protocol": "openid-connect",
                    "protocolMapper": "oidc-usermodel-realm-role-mapper"
                },
                {
                    "config": {
                        "access.token.claim": "True",
                        "claim.name": "clientHost",
                        "id.token.claim": "True",
                        "jsonType.label": "String",
                        "user.session.note": "clientHost"
                    },
                    "consentRequired": False,
                    "name": "Client Host",
                    "protocol": "openid-connect",
                    "protocolMapper": "oidc-usersessionmodel-note-mapper"
                },
                {
                    "config": {
                        "access.token.claim": "True",
                        "claim.name": "clientId",
                        "id.token.claim": "True",
                        "jsonType.label": "String",
                        "user.session.note": "clientId"
                    },
                    "consentRequired": False,
                    "name": "Client ID",
                    "protocol": "openid-connect",
                    "protocolMapper": "oidc-usersessionmodel-note-mapper"
                }
            ],
            "publicClient": False,
            "redirectUris": [
                "http://*"
            ],
            "serviceAccountsEnabled": True,
            "standardFlowEnabled": True,
            "surrogateAuthRequired": False,
            "webOrigins": []
        }
        params_path = {"realm-name": realm_name}
        ret = self.post_to_keycloak(url=URL_ADMIN_CLIENTS.format(**params_path),
                                    payload=payload)
        return ret

    def get_client_secret_in_realm(self, client_name, realm_name):
        """
        Gets a client secret of a client in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: The client secret
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        params_path = {"realm-name": realm_name, "id": client_id}
        ret = self.get_from_keycloak(url=URL_ADMIN_CLIENT_SECRETS.format(**params_path))
        return ret['value']

    def get_authz_role_policies_in_realm(self, client_name, realm_name):
        """
        Gets the Authorization role policies of a client in a realm
        :param client_name: The client name
        :param realm_name: The realm name
        :return: List[PolicyRepresentation] The list of role policies of the realm
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name,
                                                realm_name=realm_name)
        params_path = {"client-id": client_id, "realm-name": realm_name}
        ret = self.get_from_keycloak(url=URL_ADMIN_AUTHZ_POLICY.format(**params_path))
        return ret

    def get_client_authz_config_in_realm(self, client_name, realm_name):
        """
        Gets the Authorization settings of a client in a realm
        :param client_name: The name of the client
        :param realm_name: The realm name
        :return: dict The Authorization settings
        """
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name=client_name, realm_name=realm_name)
        params_path = {"realm-name": realm_name, "id": client_id}
        ret = self.get_from_keycloak(URL_ADMIN_CLIENT_AUTHZ_SETTINGS.format(**params_path))
        return ret

    def create_client_scope_in_realm(self, scope_name, realm_name):
        """
        Creates a client scope in a realm
        :param scope_name: The scope name
        :param realm_name: The realm name
        :return: ClientScopeRepresentation The generated client scope
        """
        realm_name = realm_name if realm_name else self.realm
        params_path = {"realm-name": realm_name}
        payload = {"attributes": {"display.on.consent.screen": "true", "include.in.token.scope": "true"},
                   "name": scope_name,
                   "protocol": "openid-connect"}
        ret = self.post_to_keycloak(url=URL_ADMIN_CLIENT_SCOPES.format(**params_path),
                                    payload=payload)
        return ret

    def assign_realm_roles_to_user_in_realm(self, username, roles, realm_name):
        """
        Assigns realm roles to a user
        :param username: The username of the user
        :param roles: List[str] The roles of interest
        :param realm_name: The realm name
        :return: Nothing
        """
        realm_name = realm_name if realm_name else self.realm
        user_id = self.get_user_id_in_realm(username=username, realm_name=realm_name)
        all_roles = self.get_realm_roles_in_realm(realm_name=realm_name)
        roles_repr = [x for x in all_roles if x['name'] in roles]
        payload = roles_repr
        params_path = {"realm-name": realm_name, "id": user_id}
        ret = self.post_to_keycloak(url=URL_ADMIN_USER_REALM_ROLES.format(**params_path),
                                    payload=payload)
        return ret

    def create_role_scope_mappings_in_realm(self,
                                            scope_name,
                                            role_name,
                                            realm_name):
        """
        Creates Role Scope mappings (at realm level, not client) in a realm
        :param scope_name: The scope name
        :param role_name: The role name
        :param realm_name: The realm name
        :return: Nothing (keycloak does not return anything)
        """
        realm_name = realm_name if realm_name else self.realm
        scope_id = self.get_scope_id_in_realm(scope_name=scope_name,
                                              realm_name=realm_name)
        role_ids = [x['id'] for x in self.get_realm_roles_in_realm(realm_name=realm_name) if x['name'] == role_name]
        if len(role_ids) != 1:
            log.error("Cannot find role with name {}".format(role_name))
            return None
        role_id = role_ids[0]
        payload = [{"id": role_id,
                    "name": role_name,
                    "composite": False,
                    "clientRole": False,
                    "containerId": realm_name}]
        params_path = {"realm-name": realm_name, "scope-id": scope_id}
        ret = self.post_to_keycloak(url=URL_ADMIN_SCOPE_MAPPINGS.format(**params_path),
                                    payload=payload)
        return ret

    def get_scope_id_in_realm(self, scope_name, realm_name):
        """
        Gets the ID of a scope in a realm
        :param scope_name: The scope name
        :param realm_name: The realm name
        :return: [str] The scope ID if exists, else None
        """
        realm_name = realm_name if realm_name else self.realm
        all_scopes = self.get_scopes_in_realm(realm_name=realm_name)
        for scope in all_scopes:
            if scope['name'] == scope_name:
                return scope['id']
        return None

    def get_scopes_in_realm(self, realm_name):
        """
        Fetches all the scopes of the realm
        :param realm_name: The realm name
        :return: List[ClientScopeRepresentation] of all the scopes
        """
        realm_name = realm_name if realm_name else self.realm
        params_path = {"realm-name": realm_name}
        ret = self.get_from_keycloak(URL_ADMIN_CLIENT_SCOPES.format(**params_path))
        return ret

    def create_default_protocol_mappers_in_realm(self, realm_name, client_id, client_name, client_secret):
        """
        Creates the default protocol mappers for assigning in the user token
        the realm and client roles of the user
        :param realm_name: The name of the realm to create the default protocol mappers
        :param client_id: The id of the client
        :param client_name: The name of the client
        :return: None
        """
        payload = [{
                "config": {
                    "access.token.claim": "true",
                    "claim.name": "client_roles",
                    "id.token.claim": "true",
                    "jsonType.label": "String",
                    "multivalued": "true",
                    "userinfo.token.claim": "true",
                    "usermodel.clientRoleMapping.clientId": "{}".format(client_name),
                },
                "consentRequired": False,
                "name": "client_roles",
                "protocol": "openid-connect",
                "protocolMapper": "oidc-usermodel-client-role-mapper"
            }]
        realm_name = realm_name if realm_name else self.realm
        client_id = self.get_client_id_in_realm(client_name, realm_name) if not client_id else client_id
        params_path = {"realm-name": realm_name,
                       "client-id": client_id}
        api_client = KeycloakOpenID(server_url=settings.KEYCLOAK['url'],
                                    client_id=client_name,
                                    client_secret_key=client_secret,
                                    realm_name=realm_name,
                                    verify=True)
        token = api_client.token(username=settings.KEYCLOAK['admin_username'],
                                 password=settings.KEYCLOAK['admin_password'])
        ret = requests.post(url=settings.KEYCLOAK['url'] + URL_ADMIN_CREATE_PROTOCOL_MAPPERS.format(**params_path),
                            headers={"Content-Type": "application/json",
                                     "Authorization": "Bearer {}".format(token['access_token'])},
                            json=payload
                            )
        ret.raise_for_status()
        return ret
