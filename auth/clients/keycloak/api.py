from dataclasses import dataclass

from django.conf import settings

from keycloak import KeycloakOpenID


@dataclass
class Client(object):
    client: KeycloakOpenID = None

    def __init__(self):
        self.client = KeycloakOpenID(server_url=settings.KEYCLOAK['url'],
                                     client_id=settings.KEYCLOAK['client_id'],
                                     client_secret_key=settings.KEYCLOAK['client_secret'],
                                     realm_name=settings.KEYCLOAK['realm_name'],
                                     verify=True)

    def get_client(self):
        return self.client
