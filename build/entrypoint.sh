#!/usr/bin/env sh

echo "Waiting for DB to spawn..."
while ! nc -z ${POSTGRES_HOST} 5432; do
  sleep 0.1
done
echo "PostgreSQL started"

pipenv run python manage.py migrate
pipenv run python manage.py init
cd /root

exec "$@"

exit 0