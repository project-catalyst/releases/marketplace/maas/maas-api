from dataclasses import dataclass
from typing import Any
import yaml
from kubernetes.client import V1Namespace, V1ObjectMeta

@dataclass
class KObject:
    yaml_path: str
    namespace: str
    def __post_init__(self):
        self.o = self.load_from_yaml()
        
    def get_object(self):
        return self.o

    def insert_attributes(self, attrs, sep="."):
        for key_path, val in attrs.items():
            self.insert_attribute(key_path, val)

    def insert_attribute(self, path, value, sep="."):
        if not value:
            return
        path_list = path.split(sep)
        ptr = self.o
        for step in path_list[:-1]:
            try:
                step = int(step)
            except ValueError:
                pass
            ptr = ptr[step]
        ptr[path_list[-1]] = value

    def load_from_yaml(self):
        with open(self.get_yaml_path(), "r") as yaml_file:
            return yaml.safe_load(yaml_file)

    def get_yaml_path(self):
        return self.yaml_path


@dataclass
class Namespace(KObject):

    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.namespace)

        
@dataclass
class Deployment(KObject):
    
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.namespace", self.namespace)

    def add_env_to_container(self, container_name, env):
        containers = self.o['spec']['template']['spec']['containers']
        container = [c for c in containers if c['name'] == container_name][0]
        container['env'].append(env)

@dataclass
class ConfigMap(KObject):

    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.namespace", self.namespace)

@dataclass
class PersistentVolume(KObject):
    metadata_name: str
    host_path: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("spec.hostPath.path", self.host_path)

@dataclass
class PersistentVolumeClaim(KObject):
    metadata_name: str
    volume_name: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("metadata.namespace", self.namespace)
        self.insert_attribute("spec.volumeName", self.volume_name)

@dataclass
class Service(KObject):
    metadata_name: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("metadata.namespace", self.namespace)

@dataclass
class Ingress(KObject):
    metadata_name: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("metadata.namespace", self.namespace)

@dataclass
class Secret(KObject):
    metadata_name: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("metadata.namespace", self.namespace)

@dataclass
class Job(KObject):
    metadata_name: str
    def __post_init__(self):
        super().__post_init__()
        self.insert_attribute("metadata.name", self.metadata_name)
        self.insert_attribute("metadata.namespace", self.namespace)
    