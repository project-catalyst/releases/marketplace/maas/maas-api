#!/bin/bash
date
microk8s.kubectl delete -f ingress
microk8s.kubectl delete -f services
microk8s.kubectl delete -f deployments
microk8s.kubectl delete -f secrets
microk8s.kubectl delete -f configmaps
microk8s.kubectl delete -f storage/marketplace-database-pvc.yaml
microk8s.kubectl delete -f storage/marketplace-database-pv.yaml
microk8s.kubectl delete -f jobs
microk8s.kubectl delete -f namespaces
date
