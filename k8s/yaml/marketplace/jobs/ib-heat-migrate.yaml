apiVersion: batch/v1
kind: Job
metadata:
  name: information-broker-heat-migrations
  namespace: namespace-placeholder
spec:
  ttlSecondsAfterFinished: 60
  template:
    spec:
      containers:
        - name: information-broker-heat-migrations
          image: h2020catalyst/marketplace-information-broker:latest
          imagePullPolicy: Always
          command: ['pipenv', 'run', 'python', 'manage.py', 'migrate']
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: heat-database-credentials
                  key: user

            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: heat-database-credentials
                  key: password

            - name: POSTGRES_DATABASE
              valueFrom:
                secretKeyRef:
                  name: heat-database-credentials
                  key: db

            - name: POSTGRES_HOST
              value: information-broker-heat-database-service

            - name: DJANGO_SETTINGS_MODULE
              value: information_broker.settings.production
      initContainers:
        - name: wait-for-db
          image: busybox:latest
          command: ['sh', '-c', "until nc -w 1 -z information-broker-heat-database-service 5432; do echo waiting for information-broker-heat-database-service to start; sleep 1; done"]

      restartPolicy: Never
  backoffLimit: 5
