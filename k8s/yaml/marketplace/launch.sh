#!/bin/bash

date 
find -depth -type f -exec sed -i "s/namespace-placeholder/$1/g" {} \;
microk8s.kubectl apply -f namespaces
microk8s.kubectl apply -f storage
microk8s.kubectl apply -f secrets
microk8s.kubectl apply -f configmaps
microk8s.kubectl apply -f deployments
microk8s.kubectl apply -f services
microk8s.kubectl apply -f ingress
microk8s.kubectl apply -f jobs/migrate.yaml
microk8s.kubectl apply -f jobs/create_superuser.yaml
microk8s.kubectl apply -f jobs/init_db.yaml
date
