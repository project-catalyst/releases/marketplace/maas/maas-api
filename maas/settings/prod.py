from .base import *

DEBUG = True

# ==================================
#   DATABASE SETTINGS
# ==================================
POSTGRES_DATABASE = os.getenv('POSTGRES_DATABASE', default='maas')
POSTGRES_USER = os.getenv('POSTGRES_USER', "maas")
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', "maas")
POSTGRES_HOST = os.getenv('POSTGRES_HOST', "db")
POSTGRES_PORT = os.getenv('POSTGRES_PORT', default=5432)

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.postgresql',
        'NAME'    : POSTGRES_DATABASE,
        'USER'    : POSTGRES_USER,
        'PASSWORD': POSTGRES_PASSWORD,
        'HOST'    : POSTGRES_HOST,
        'PORT'    : POSTGRES_PORT,
    }
}

DEPLOYMENT_DEFAULTS = {
    "DATA_PV_DIRECTORY": os.getenv("DATA_PV_DIRECTORY", "/data"),
    "EXTERNAL_URL": os.getenv("EXTERNAL_URL", "localhost"),
    "MSM_URL": os.getenv("MSM_URL", ""),
    "MCM_URL": os.getenv("MCM_URL", ""),
    "ITLB_URL": os.getenv("ITLB_URL", ""),
    "DCMCM_URL": os.getenv("DCMCM_URL", ""),
    "VCG_URL": os.getenv("VCG_URL", ""),
}

KEYCLOAK =  {
    "url": os.getenv("KEYCLOAK_URL", "http://maas-keycloak-service:8080/auth/"),
    "client_id": os.getenv("KEYCLOAK_CLIENT_ID", "catalyst-maas"),
    "client_secret": os.getenv("KEYCLOAK_CLIENT_SECRET", "bfa7db47-e19c-4772-95c3-d26bda19d8f2"),
    "realm_name": os.getenv("KEYCLOACK_REALM_NAME", "CATALYST"),
    "config": os.path.join(BASE_DIR, 'keycloak-config-prod.json'),
    "admin_username": os.getenv("KEYCLOAK_ADMIN_USERNAME", "admin"),
    "admin_password": os.getenv("KEYCLOAK_ADMIN_PASSWORD", "nQc5NqEwPcJk3wG7amnbd9X0P4Kx6G7O"),
    "marketplace_realm_name": os.getenv("KEYCLOAK_MARKETPLACE_REALM_NAME", "CATALYST Marketplace Federation")
}