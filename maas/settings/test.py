from .base import *

DEBUG = True

# ==================================
#   DATABASE SETTINGS
# ==================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'maas.db',
    }
}

KEYCLOAK =  {
    "url": os.getenv("KEYCLOAK_URL", "http://192.168.1.138:30308/auth/"),
    "client_id": os.getenv("KEYCLOAK_CLIENT_ID", "catalyst-maas"),
    "client_secret": os.getenv("KEYCLOAK_CLIENT_SECRET", "bfa7db47-e19c-4772-95c3-d26bda19d8f2"),
    "realm_name": os.getenv("KEYCLOACK_REALM_NAME", "CATALYST"),
    "config": os.path.join(BASE_DIR, 'keycloak-config-prod.json'),
    "admin_username": os.getenv("KEYCLOAK_ADMIN_USERNAME", "admin"),
    "admin_password": os.getenv("KEYCLOAK_ADMIN_PASSWORD", "nQc5NqEwPcJk3wG7amnbd9X0P4Kx6G7O"),
    "marketplace_realm_name": os.getenv("KEYCLOAK_MARKETPLACE_REALM_NAME", "CATALYST Marketplace Federation")
}

PRESERVE_TEST_DEPLOYMENTS = True