from django.contrib import admin
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

urlpatterns = [
    path('catalyst/maas/admin/', admin.site.urls),
    path('catalyst/maas/api/v1/', include('api.urls'))
]

# urlpatterns += staticfiles_urlpatterns()
